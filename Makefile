CC = gcc
#CC = clang
CCWin64 = x86_64-w64-mingw32-gcc
CFlAGS = -Wall -O3
dir = Source

crypt: $(dir)/uint_arithmetic.o $(dir)/crypt.o $(dir)/test_crypt.o compatibility_check Makefile
	$(CC) $(CFlAGS) $(dir)/uint_arithmetic.o $(dir)/crypt.o  $(dir)/test_crypt.o -o crypt

$(dir)/uint_arithmetic.o: $(dir)/uint_arithmetic.c $(dir)/uint_arithmetic.h $(dir)/mod_division.c Makefile
	$(CC) $(CFlAGS) -c $(dir)/uint_arithmetic.c -o $(dir)/uint_arithmetic.o
	
# To simple for the above:
%.o: %.c %.h Makefile
	$(CC) $(CFlAGS) -c $*.c -o $@

compatibility_check: $(dir)/compatibility_check.c Makefile
	$(CC) $(CFlAGS) $(dir)/compatibility_check.c -o compatibility_check
	@./compatibility_check

Win64:
	$(CCWin64) $(CFlAGS) $(dir)/test_crypt.c $(dir)/crypt.c $(dir)/uint_arithmetic.c -o crypt.exe
	$(CCWin64) $(CFlAGS) $(dir)/compatibility_check.c -o compatibility_check.exe

debug:
	gcc -g $(dir)/crypt.c $(dir)/test_crypt.c $(dir)/uint_arithmetic.c -o crypt

clang:
	clang $(CFlAGS) $(dir)/crypt.c $(dir)/test_crypt.c $(dir)/uint_arithmetic.c -o crypt

test: crypt
	@ echo "Updated executable.\n"
	@./crypt --fulltest

profile:
	gcc -pg $(CFlAGS) $(dir)/crypt.c $(dir)/test_crypt.c $(dir)/uint_arithmetic.c -o crypt
	@./crypt --test 1000 --load
	gprof crypt

clean:
	rm $(dir)/*.o
	rm compatibility_check
	
