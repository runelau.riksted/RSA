#include <stdio.h>
#include <stdint.h>

int main(){
	// Checks for, that the computer is compatible with the pointer and integer operations used in the program
	
	uint32_t maxi = ~0;
	uint64_t j = (uint64_t)10*maxi+10 ;
	if(sizeof(uint64_t) != 2*sizeof(uint32_t)){
		printf("This program is garbage on you machine, because sizeof(uint64_t) != 2*sizeof(uint32_t)\n (%u != 2*%u)\n\n\n", (uint32_t) sizeof(uint64_t), (uint32_t) sizeof(uint32_t));
		return 1;
	}
	if((maxi-1)/2+1 !=1<<(sizeof(uint32_t)*8-1) ){
		int byte_size = 1;
		while((maxi>>(sizeof(uint32_t)*byte_size-1))!=1){
			/*printf("byte_size>=%d\n", byte_size);
			printf("(maxi>>byte_size)=%x\n", (maxi>>byte_size));*/
			byte_size ++;
		}
		printf("This program is garbage on you machine, because you apparently have %d bits per byte, I want 8.\n", byte_size);
		return 1;
	}
	if ( *((uint32_t *)&j + 1) != 10|| 10!=j>>8*sizeof(uint32_t)){
		
		printf("This program is garbage on you machine. (I don't like the format your ints are stored in; I can't do my pointer gymnastics.)\n");
		return 1;
	
	}
	printf("Passed compatibility check.\n");
	return 0;
}
