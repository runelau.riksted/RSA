#ifndef uint_arithmetic
#define uint_arithmetic
#include <stdint.h>

/*This library defines arithmetic operations for very large uint32_t integers.
	The uint32_t integer A is represented as uint32_t A[lenA] with  
	A = sum( A[i]*2^(i * sizeof(uint32_t)*8) ) for 0<=i<lenA
	
	The descriptions of the functions bellow are written, 
	as the equvalent statement for simple uint32_t numbers (assuming no under overflows)
	, but A^B is A to the power of B.
	
	The integer inputs of the form "len****", are the lengths of the input "****". i.e. I would be represented as uint32_t I[lenI]*/

// Print v in a nice readable format using %x
uint32_t printv(int const lenv, const uint32_t v[lenv]);

/*Returns the lenA minus the number of leading zero blocks.*/
uint32_t sig_len(const int lenA, const uint32_t *A);

/*returns 0 if A<B, 1 if A=B, 2 if A>B, */
uint32_t compare(const int len, const uint32_t *A, const uint32_t *B);

/* I := I>>(d*32 +r), 0 <= r < 32 */
void right_shift_array(uint32_t lenI, uint32_t *I, int d, int r);

/* I := I<<(d*32 +r), 0 <= r < 32 */
void left_shift_array(uint32_t lenI, uint32_t *I, int d, int r);

/* I := I*2^n */
uint32_t mult2n(const uint32_t lenI, uint32_t *I, const int n);

/* A := A + B, returns the value that should have been assigned to A[M] by an overflow.*/
uint32_t add_array(const int len, uint32_t *A, const uint32_t *B);

/* A := A - B, returns 1 if the A<B, but A stays underflowed*/
uint32_t sub_array(const int len, uint32_t *A, const uint32_t *B);

/* C := A*B, if lenA*lenB>lenC, then C := truncated(A*B)*/
uint32_t mult_array(const int lenA, const uint32_t *A, int const lenB, const uint32_t *B, const uint32_t lenC, uint32_t *C);

/*Used for efficiently performing a division by self in div_array(...) or a number modulo by self in mod(...).*/
typedef struct ModKit{
	int len;					// Lenght of self minus without the leading 0 entries
	int pow;					// Number of leading zeros in self[len-1]
	int uncer;					//=1 if self[len-2] has a leading zero, =2 if self[len-2] has a leading one
	const uint32_t *  self;		// The modulus
	const uint32_t *  shift;	// self shifted left/up to have no leading zeros in self[len-1]
	uint32_t * mod2n;			//=2**( (sizeof(uint32_t)*(modn + len) ) (mod self) )
	
} ModKit;
#define modn 6// The value to use for n in ModKit above to calculate mod2n.

/*Calculate the ModKit corresponding to MOD, MOD_Data[2*M] is used to store ModKit Data.*/
ModKit mod_prep(int lenMOD, const uint32_t *MOD, uint32_t *MOD_Data);

/*Same as mod_prep(...), but it dinamically allocates memory for the MOD_DATA.
	Remember to call free(Modkit.shift), but not free(Modkit.mod2n)*/
ModKit mod_prep_alloc(const int lenMOD, const uint32_t *MOD);

/*A := A%MOD.self 
	Definition in mod_division.c, with mod0_div1 = 0*/
uint32_t mod(int lenA, uint32_t *A, ModKit MOD );

/*D := A/MOD.self, A := A%MOD.self
	Definition in mod_division.c, with mod0_div1 = 1*/
uint32_t div_array(int lenA, uint32_t *A, ModKit MOD, int lenD, uint32_t *D );

/*B := (B^E) % MOD.self*/
uint32_t exp_mod(int lenB, uint32_t *B, int lenE, const uint32_t *E, const ModKit MOD);

/*B := (B^E) % MOD*/
uint32_t exp_mod_easy(int lenB, uint32_t *B, int lenE, const uint32_t *E, int lenMOD, const uint32_t *MOD);

/*A := A % MOD*/
uint32_t mod_easy(int lenA, uint32_t *A, int lenMOD, const uint32_t *MOD);

// res := atoi(str_in)
uint32_t hexify(char * str_in, uint32_t lenres, uint32_t *res);

/*replaces I with another integer of roughfly the same size. The reuslting integer is meant to be "hard" to predict.*/
uint32_t new_int(uint32_t lenI, uint32_t *I);

/*returns 1 => P is with high probabillity a prime, returns 0 => P is not a prime. */
uint32_t is_prime(uint32_t lenP, uint32_t *P);

/*Sets a and Gcd, such that Gcd = a*A + b*B, 
	returns number of uint32_ts used to represent Gcd, not counting leading zeros. */
uint32_t ext_euclid(const uint32_t len, const uint32_t * const A, const uint32_t * const B, uint32_t * const a, int lenGcd, uint32_t * const Gcd);

// a:= A^-1 (mod B), in the sence that a*A = 1 (mod B)
uint32_t invert_array(const uint32_t len, const uint32_t * const A, const uint32_t * const B, uint32_t * const a);
#endif
