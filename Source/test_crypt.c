#include <time.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "uint_arithmetic.h"
#include "crypt.h"

clock_t mean, var, start, diff;
double meand, vard;
int timing_laps= 0;

#define start_timing() {\
    mean=0; var=0; timing_laps = 0;\
}

#define start_timing_lap() {\
    start = clock();\
}

#define end_timing_lap() {\
    diff = clock() - start;\
    mean += diff;\
    var += diff*diff;\
    timing_laps ++;\
}

#define end_timing() {\
    meand = ((double) mean)/timing_laps / CLOCKS_PER_SEC;\
    vard = ( (double) var)/ CLOCKS_PER_SEC / CLOCKS_PER_SEC;\
    vard -= meand*meand*timing_laps;\
    vard /= timing_laps - 1;\
}

int test_raw_key(PublicKey* pub_key, int lenPQ, uint32_t *priv_exp){

// Test Whether the raw key works (x^(priv_key*pub_exp) == x (mod PQ) )
    uint32_t Msg[lenPQ], Msg_cpy[lenPQ];
    
    memset(Msg, 0, sizeof(*Msg)*lenPQ);
    Msg[0] = 0x12345678;
    Msg[lenPQ/2] = 0x12345678;
    Msg[lenPQ-2] = 0x12345678;
    
    memcpy(Msg_cpy, Msg, sizeof(*Msg)*lenPQ);
    
    exp_mod(lenPQ, Msg, pub_key->lenExp, pub_key->Exp, pub_key->PQ_mod);
    exp_mod(lenPQ, Msg, lenPQ, priv_exp, pub_key->PQ_mod);
        
    if ( compare(lenPQ, Msg, Msg_cpy) != 1){
        printf("The generated raw key, didn't work.\nMsg:\n");
        printv(lenPQ, Msg);
        
        printf("Private exponent:\n");
        printv(lenPQ, priv_exp);
        
        printf("Public exponent:\n");
        printv( pub_key->lenExp, pub_key->Exp);
        
        return 1;
    }
    
    return 0;
}

int is_prime_candidate_strong(int lenP, uint32_t *P, int lenQ, uint32_t *Q, uint32_t *q){
    /*Test that P is "strong".*/
    uint32_t A[lenP];
    int has_failed = 0;
    
    memcpy(A, P, sizeof(P[0])*lenP);
    A[0] --;
    mod_easy(lenP, A, lenQ, Q);
    
    if( sig_len(lenP, A) != 0 ){
        printf("(P-1)%%Q != 0, P:\n");
        printv(lenP, A);
        has_failed = 1;
        
    }
    
    memcpy(A, P, sizeof(P[0])*lenP);
    A[0] ++;
    mod_easy(lenP, A, lenQ, q);
    
    if( sig_len(lenP, A) != 0 ){
        printf("(P+1)%%q != 0, P:\n");
        printv(lenP, A);
        has_failed = 2;
        
    }
    
    if( mod_30(lenP, P) != 1){
        printf("P%%30 = %d != 1\n", mod_30(lenP, P) );
        has_failed = 3;
    }
    
    memset(A, 0, sizeof(*A)*lenP);
    
    return has_failed;
}

// Should just import <math.h>, but I want to use as few libraries as possible...
double sqrt(double x){
    double S = x, r = x/2;
    
    while ( ( r*r>= S && (r*r/S-1) > 0.0001 ) || ( r*r< S && (1 -r*r/S) > 0.0001 ) ){
        for (int i=0; i<100; i++){
            r= (S/r+r)/2;
        }
    }
    return r;
}

int test_Q_inv(PrivateKey *priv_key){
    // Test Q_inv
    uint32_t  Key_size = priv_key->PQ_mod.len, Product[Key_size];
    memset(Product, 0, sizeof(uint32_t)*Key_size);
    
    mult_array(priv_key->lenP, priv_key->Q_inv, priv_key->lenQ, priv_key->Q, Key_size, Product);
    
    mod(Key_size, Product, priv_key->P_mod);
    
    if ( sig_len(Key_size, Product) != 1 || Product[0] != 1 ){
        
        printf("Q:\n");
        printv(priv_key->lenP, priv_key->Q);
        
        printf("Q_inv:\n");
        printv(priv_key->lenP, priv_key->Q_inv);
        
        printf("Q*Q_inv (mod P):\n");
        printv(Key_size, Product);
        
        return 1;
    }
    
    return 0;
}


int test_prime(int lenP, uint32_t *P, char * label){
    
    if (is_prime(lenP, P)==0){
        printf("\n%s isn't a Prime!!!\n", label);
        printf("%s[%d]=\n", label, lenP);
        printv(lenP, P);
        return 1;
    }
    
    return 0;
}
void create_test_message(int lenMsg, uint32_t Msg[lenMsg]){
    
    #if 1
        #if _WIN64
            //x86_64-w64-mingw32-gcc messes up special characters.
            char*  test_message = "This message was encrypt and decrypt successfully if you are reading this.\n";
        #else
            char*  test_message = "This message was encrypt and decrypt successfully if you are reading this 😁\n";
        #endif
        
        // Fill the Msg array by repeating test_message over and over:
        uint64_t step = strlen(test_message)*sizeof(char), offset = 0;
        
        for(; offset+step< (lenMsg-1)*sizeof(uint32_t); offset+= step){
            memcpy( ((void*) Msg) + offset , test_message, step);
        }
        step = (lenMsg-1)*sizeof(uint32_t) - offset;
        memcpy( ((void*)Msg) + offset , test_message, step-sizeof(char));
        
        Msg[lenMsg-1] = (uint32_t)'\0';
        
    #else
        
        // Use a sparse test message
        memset(Msg, 0, sizeof(uint32_t)*lenMsg);
        Msg[0] = 0x12345678;
        Msg[10] = 0x12345678;
        Msg[lenMsg-2] = 0x12345678;
    
    #endif
    
    #if 0
        printf("Message:\n");
        printv(lenMsg, Msg);
    #endif
}

void time_block_encryption_decryption(PublicKey *pub_key, PrivateKey *priv_key, int test_repeats){
// Create a test message of suitable length:
    int Key_size = pub_key->PQ_mod.len;
    uint32_t Msg[Key_size] /*Message*/;
    
    create_test_message(Key_size, Msg);
    // Do an untimed run, to check the output (separated out because, the printing takes time):
    encrypt_block(pub_key->PQ_mod.len, Msg, pub_key);    
    decrypt_block(priv_key->PQ_mod.len, Msg, priv_key);
    
    printf("Test message after encryption and decryption:\n");
    printf("%s\n", (char*) Msg);
    
    // Measure the execution speed.
    printf("Speed test:\n");
    start_timing();
    
    for(int i= 0; i<test_repeats; i++){
        start_timing_lap();
        
        encrypt_block(pub_key->PQ_mod.len, Msg, pub_key);
        decrypt_block(priv_key->PQ_mod.len, Msg, priv_key);
        
        end_timing_lap()
        
    }
    
    end_timing();
    
    #if _WIN64
        //x86_64-w64-mingw32-gcc messes up special characters.
        printf("One Encryption and decryption took %f +/- %f ms (averaged over %d runs.).\n", (meand)*1000, sqrt( vard )*1000, test_repeats);
    #else
        printf("One Encryption and decryption took %f ± %f ms (averaged over %d runs.).\n", (meand)*1000, sqrt( vard )*1000, test_repeats);
    #endif
    return;
}

int test_save_load_key(PublicKey *pub_key, PrivateKey *priv_key){
    
    int Key_size = priv_key->PQ_mod.len;
    
    uint32_t set_value = 0x0f, // The first 2 bytes will be repeated over and over in Msg.
             Msg[Key_size];
    memset( Msg, set_value, sizeof(Msg[0])*(Key_size-1) );
    set_value = Msg[0];
    Msg[Key_size - 1] = 0;
    
    PublicKey *pub_key2 = NULL;
    PrivateKey *priv_key2 = NULL;
    
    if(
        !save_pub_key(pub_key, "temp")
    ){
        
        pub_key2 = load_pub_key("temp");
        if(priv_key){
            
            encrypt_block(pub_key->PQ_mod.len, Msg, pub_key);
            free_PublicKey(pub_key2);
            
        }else{
            printf("Failed to load PublicKey!\n");
            return 1;
        }
        remove("temp.pub");
    }else{
        printf("Failed to save PublicKey!\n");
        return 1;
    }
    
    
    if(
        !save_priv_key(priv_key, "temp")
    ){
        
        priv_key2 = load_priv_key("temp");
        if(priv_key){
            
            decrypt_block(priv_key->PQ_mod.len, Msg, priv_key);
            free_PrivateKey(priv_key2);
            
        }else{
            printf("Failed to load PrivateKey!\n");
            return 1;
        }
        remove("temp.priv");
        
    }else{
        printf("Failed to save PrivateKey!\n");
        return 1;
    }
    
    for(int i=0; i<Key_size; i++){
        if((Msg[i] != set_value && i != Key_size-1 )  || 
            (i == Key_size-1 && Msg[i] != 0) 
        ){
            
            printf("\n\nThe saved and loaded keys did not decrypt/encrypt correctly!\n");
            printf("Output:\n");
            printv(Key_size, Msg);
            return 1;
        }
    }
    
    return 0;
}

int test_routine(PublicKey *pub_key, PrivateKey *priv_key, int test_repeats){
    
    if ( pub_key == NULL || priv_key == NULL){
        return 1; 
    }
    
    int Key_size = priv_key->PQ_mod.len;
    
    if( test_prime(priv_key->lenP, priv_key->P, "P") ||
        test_prime(priv_key->lenQ, priv_key->Q, "Q")
    ){
        return 1;
    }
    
    if(
        test_Q_inv(priv_key)
    ){
        return 1;
    }
    
    if( Key_size != pub_key->PQ_mod.len ){ 
        return 1;
    }
    
    time_block_encryption_decryption(pub_key, priv_key, test_repeats);
    
    if(
        test_save_load_key(pub_key, priv_key)
    ){
        return 1;
    }
    
    return 0;
}
