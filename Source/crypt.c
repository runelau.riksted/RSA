#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "uint_arithmetic.h"
#include "test_crypt.h"
#include <time.h>

void free_clear_uint32_t_array(int lenA, const uint32_t A[lenA]){
    memset((uint32_t  *) A, 0, sizeof(uint32_t)*lenA);
    free((uint32_t  *) A );
}

PrivateKey * malloc_PrivateKey(int lenP, int lenQ){

    PrivateKey * priv_key = malloc(sizeof(PrivateKey));
    
    priv_key->lenP = lenP;
    priv_key->lenQ = lenQ;
    
    priv_key->P = malloc(sizeof(uint32_t)*lenP);
    priv_key->Q = malloc(sizeof(uint32_t)*lenQ);
    priv_key->Q_inv = malloc(sizeof(uint32_t)*lenP);
    
    priv_key->Exp_p = malloc(sizeof(uint32_t)*lenP);
    priv_key->Exp_q = malloc(sizeof(uint32_t)*lenQ);
    
    return priv_key;
}

PublicKey * malloc_PublicKey(int lenExp){

    PublicKey * pub_key = malloc(sizeof(PublicKey));
    
    pub_key->Exp = malloc(sizeof(uint32_t)*lenExp);
    pub_key->lenExp = lenExp;

    return pub_key;
}

void free_PrivateKey(PrivateKey * key){
    free_clear_uint32_t_array(key->lenP, key->P);
    free_clear_uint32_t_array(key->lenQ, key->Q);
    free_clear_uint32_t_array(key->lenP, key->Q_inv);
    free_clear_uint32_t_array(key->lenP, key->Exp_p);
    free_clear_uint32_t_array(key->lenQ, key->Exp_q);
    
    //free_clear_uint32_t_array((key->P_mod).self); - Omitted since (key->P_mod).self is key->P.
    free_clear_uint32_t_array(key->lenP*2, (key->P_mod).shift);
    
    //free_clear_uint32_t_array((key->Q_mod).self); - Omitted since (key->Q_mod).self is key->Q.
    free_clear_uint32_t_array(key->lenQ*2, (key->Q_mod).shift);
    
    free_clear_uint32_t_array((key->PQ_mod).len*2, (key->PQ_mod).shift);
    free_clear_uint32_t_array((key->PQ_mod).len, (key->PQ_mod).self);
    
    
    memset((PrivateKey *) key, 0, sizeof(PrivateKey));
    free(key);
}

void free_PublicKey(PublicKey * key){
    free(key->Exp);
    free((void*) (key->PQ_mod).self);
    free((void*) (key->PQ_mod).shift);
    free(key);
    
}

#define key_length(KEY) (((KEY)->PQ_mod).len)
#define get_block_size(KEY) (sizeof(uint32_t)*key_length(KEY) - 1 - (((KEY)->PQ_mod).pow+1)/8)

uint32_t mod_30(int lenP, uint32_t P[lenP]){
    
    int mod30=P[0]%30;
    
    for(int i=1; i<lenP; i++){
        mod30 += (P[i]%30) * 16; // works since 2^(4*n) = 16 (mod 30) for n>1.
    }
    
    return mod30%30;
}

uint32_t gen_prime(int lenP, uint32_t P[lenP]){
    /* 
     * Pre calculate the distance between the numbers relatively prime to 30=2*3*5,
     * in order to skip all numbers divisible by 2, 3, and 5 (22/30~3/4 of all numbers).
     */
    
    int Deltas[8], 
        prime_to_30[8] = {1,7,11,13,17,19,23,29}, 
        Pmod30 = mod_30(lenP, P),
        j = 0;
    
    for (int i =0; i< 8; i++){
    
        if (Pmod30>prime_to_30[i]){
            j = i; 
        }
        
        Deltas[i] = (30+prime_to_30[(i+1)%8]-prime_to_30[i])%30;
    }
     
    /* Round P down to the closest number, which is relatively prime to 30.*/
    P[0] += prime_to_30[j] - Pmod30;
    
    for(; j<100000000; j++){
        if (is_prime(lenP, P)){
            
            return 0;
        }
        P[0] += Deltas[j%8];
    }
    
    return 1;
}

#define verbose_gen_strong_prime 1
void gen_strong_prime_candidate(int lenQ, uint32_t nxqQ[3][lenQ*2+2], int lenP, uint32_t P[lenP]){
    /*
     *    The goal is to calculate a first candidate integer P, where
     *    P ≡ -1     (mod q),    for a large prime q
     *    P ≡ 1     (mod Q),    for a large prime Q
     *    P ≡ 1     (mod 30).
     *  P+1 and P-1 are chosen to have large prime factors, 
     *  too guard against Pollard's P-1 and Williams's p + 1 algorithms.
     *    The requirement of P ≡ 1 (mod 30) is not necessary for P to be a strong prime,
     *    but if P%30 is known, one can simply add the necessary multiple of qQ, to skip all numbers
     *    divisible by 2,3 and 5, thus skipping 1 -φ(30)/30 ~ 3/4 of all numbers.
     *    Satisfying the congruence relations is achieved with the Chinese remainder theorem, which gives:
     *    P = 2*q^(-1)*q -1 + (31 - [2*q^(-1)*q -1]%30 ) * (qQ%30)^-1 * qQ
     *    
     *    Adding any multiple of 30*q*Q to P will preserve the congruence relations.
     */
    
    uint32_t     * const temp=nxqQ[0], 
                * const q=nxqQ[1], 
                * const Q=&nxqQ[1][lenQ+1], 
                * const qQ = nxqQ[2],
                lenqQ = lenQ*2;
    
    memcpy(Q, P, sizeof(uint32_t)*(lenQ));
    gen_prime(lenQ, Q);
    
    memcpy(q, &P[lenQ], sizeof(uint32_t)*(lenQ));
    gen_prime(lenQ, q);
    
    mult_array(lenQ, q, lenQ, Q, lenqQ, qQ);
    
    //P := q*Q*N, for large random N
    mult_array(lenqQ, qQ, lenP-lenqQ, &P[lenqQ], lenP, P);
    
    // temp := q^(-1)
    invert_array(lenQ, q, Q, temp);
    
    // temp := 2*q^(-1)*q -1 
    mult_array(lenQ, temp, lenQ, q, lenqQ, temp);
    temp[lenqQ] = 0;
    mult2n(lenqQ+1, temp, 1);
    temp[0] --;
    
    // P += 2*q^(-1)*q -1 Making P ≡ -1 (mod q),    P ≡  1 (mod Q)
    add_array(lenqQ + 1, P, temp);
    
    uint32_t qQ_inv = mod_30(lenqQ, qQ),  thirty=30, n;
    
    invert_array(1, &qQ_inv, &thirty, &qQ_inv);
    
    // Set 0 <= n < 30, such that n * qQ = 1-P (mod 30).
    n = ( qQ_inv * ( 1 + 30- mod_30(lenP, P) ) )%30;
    
    // temp := q*Q * n
    mult_array(lenqQ, qQ, 1, &n, lenqQ+1, temp);
    
    //Add a muliple of qQ to P, such that P = 1 (mod 30).
    add_array(lenqQ+1, P, temp);
    
    #if verbose_gen_strong_prime
        is_prime_candidate_strong(lenP, P, lenQ, Q, q);
    #endif
    
    // nxqQ[i]%30 := 2*(i+1)
    for (uint32_t i=0; i<3; i++){
        n = (qQ_inv * 2*(i+1) )%30;
        mult_array( lenqQ, qQ, 1, &n, lenQ*2+1, nxqQ[i] );
        nxqQ[i][lenQ*2+1] = 0;
    }
    
    #if verbose_gen_strong_prime
        for (uint32_t i=0; i<3; i++){
            if( mod_30(lenQ*2+2, nxqQ[i]) != 2*(i+1) ){
                printf("nxqQ%%30 = %d != %d\n", mod_30(lenQ*2+2, nxqQ[0]), 2*(i+1));
                
            }
        }
    #endif
    
}

uint32_t gen_strong_prime(int lenP, uint32_t P[lenP]){
    /*
     *    The use of strong primes, is no longer considered important
     *    since the discovery of factoring algorithms like the General number field sieve.
     *    But I'm just having fun so I thought I would give it a go.
     */
    
    uint32_t     lenQ=lenP/4, n2qQ[3][lenQ*2+2];
    
    gen_strong_prime_candidate(lenQ, n2qQ, lenP, P);
    
    /* Calculate the Deltas between numbers relatively prime to 30, which will be used to skip numbers divisible 
     * by 2, 3, and 5 =22/30~3/4 of all numbers.*/
    int Deltas[8], prime_to_30[8] = {1,7,11,13,17,19,23,29};
    
    for (int i =0; i< 8; i++){
        Deltas[i] = (30+prime_to_30[(i+1)%8]-prime_to_30[i%8])%30;
        Deltas[i] = (Deltas[i]/2-1);
    }
    
    for(int j=0; j<1000000000;j++){
        if(add_array(lenQ*2+2, P, n2qQ[Deltas[j%8]])){
            printf("Prime might not be strong.\n");
        }
        
        if (is_prime(lenP, P)){
            return 0;
        }
    }
    
    return 1;
}


PrivateKey * set_private_exponents(PublicKey *pub_key, PrivateKey *priv_key, uint32_t *Pminus, uint32_t *Qminus){

    int lenP = priv_key->P_mod.len,
        lenQ = priv_key->Q_mod.len,
        lenPQ = priv_key->PQ_mod.len;
    
    uint32_t pub_exp[lenPQ], priv_exp[lenPQ];
    
    // priv_exp := (P-1)(Q-1)
    mult_array(priv_key->lenP, Pminus, priv_key->lenQ, Qminus, lenPQ, priv_exp);
    
    memset(pub_exp, 0, sizeof(*pub_exp)*lenPQ);
    memcpy(pub_exp, pub_key->Exp, sizeof(*pub_exp)*pub_key->lenExp);
    
    if (
        // priv_exp := (pub_exp)^-1 ( mod (P-1)(Q-1) )
        invert_array(lenPQ, pub_exp, priv_exp, priv_exp)
    ){
        printf("Can't find private key, change the seed or the public key.\n");
        return NULL;
    }
    
    
    pub_key->Exp = malloc(sizeof(uint32_t));
    
    memcpy(pub_key->Exp, pub_exp, sizeof(uint32_t)*pub_key->lenExp);
    
    memcpy(pub_exp, priv_exp, sizeof(uint32_t)*lenPQ);
    
    #if 1
        test_raw_key(pub_key, lenPQ, priv_exp);
    #endif
    
    mod_easy(lenPQ, priv_exp, lenP, Pminus);    // P is currently P-1
    mod_easy(lenPQ, pub_exp, lenQ, Qminus);    // Q is currently Q-1
    
    memcpy(priv_key->Exp_p , priv_exp, sizeof(uint32_t)*lenP);
    memcpy(priv_key->Exp_q , pub_exp, sizeof(uint32_t)*lenQ);
    
    memset(pub_exp, 0, sizeof(*pub_exp)*lenPQ);
    memset(priv_exp, 0, sizeof(*priv_exp)*lenPQ);
    
    return priv_key;
}

uint32_t primes_2_keys( PublicKey ** pub_key_ptr, PrivateKey ** priv_key_ptr, int lenP, uint32_t *P_in, int lenQ, uint32_t *Q_in, int lenPubExp, uint32_t * PubExp){
    
    PublicKey *pub_key;
    PrivateKey *priv_key;
    
    
    lenP = sig_len(lenP, P_in);
    lenQ = sig_len(lenQ, Q_in);
    
    if(pub_key_ptr == NULL || priv_key_ptr == NULL ||
       lenP == 0 || lenQ == 0  
       ){
        return 1;
    }
    
    if(lenPubExp > 0 &&  PubExp != NULL){
        pub_key = *pub_key_ptr = malloc_PublicKey(lenPubExp);
        memcpy(pub_key->Exp, PubExp, sizeof(PubExp[0])*lenPubExp );
        
    }else{
        pub_key = *pub_key_ptr = malloc_PublicKey(1);
        pub_key->Exp[0] = 0x10001;
    }
    
        
    priv_key = *priv_key_ptr = malloc_PrivateKey(lenP, lenQ);
    
    int lenPQ = lenP + lenQ;
    
    uint32_t *P   = priv_key->P,
             *Q   = priv_key->Q, 
             *PQ  = malloc( sizeof(uint32_t)*(lenPQ) ), 
             *PQ2 = malloc( sizeof(uint32_t)*(lenPQ) );
    
    memcpy(P, P_in, sizeof(uint32_t)*(lenP));
    memcpy(Q, Q_in, sizeof(uint32_t)*(lenQ));
    
    
    *(ModKit *) &priv_key->P_mod = mod_prep_alloc(lenP, P);
    *(ModKit *) &priv_key->Q_mod = mod_prep_alloc(lenQ, Q);    
    
    // PQ := P*Q
    mult_array(priv_key->lenP, P, priv_key->lenQ, Q, lenPQ, PQ);
    memcpy(PQ2, PQ, sizeof(uint32_t)*lenPQ);
    
    *(ModKit *) &(priv_key->PQ_mod) = mod_prep_alloc( lenPQ, PQ );
    *(ModKit *) &(pub_key->PQ_mod) = mod_prep_alloc( lenPQ, PQ2 );
    
    P[0] --;
    Q[0] --;
    
    set_private_exponents(pub_key, priv_key, P, Q);
    
    P[0] ++;// Undo the subtraction of one
    Q[0] ++;// Undo the subtraction of one
    
    
    memcpy(priv_key->Q_inv, Q, sizeof(uint32_t)*priv_key->lenQ);
    
    mod(priv_key->lenQ, priv_key->Q_inv, priv_key->P_mod);
    
    invert_array(priv_key->lenP, priv_key->Q_inv, P, priv_key->Q_inv);
    
    return 0;
}

uint32_t fill_in(uint32_t lenA, uint32_t * A){
    
    uint32_t len;
    
    len = sig_len(lenA, A);
    
    if (len == 0){;
        return 2;
    }
    
    if (A[len-1] < (~(uint32_t)0)/16){
        //Most likely A[len-1] has a lot of leading zeros, this "removes them":
        for(int i = 0; i< len-1; i++){
            A[len-1] ^= A[i];
        }
    }
    
    if(lenA != len){
        // Fill in leading zero blocks of P:
        for(int i = len; i< lenA; i++){
            A[i] += A[i%len] ^ (i*0xaf3b81ca);
            
        }
        
        if(A[lenA-1] == 0){
            A[lenA-1] = A[lenA-1];
        }
        new_int(lenA, A);
        return 1;
    }
    
    return 0;
}

uint32_t keygen( int Key_size, PublicKey ** pub_key_ptr, PrivateKey ** priv_key_ptr,  char* seed){
    
    uint32_t    P[Key_size], 
                Q[Key_size], 
                lenP= Key_size-Key_size/2, 
                lenQ= Key_size/2;
    
    memset(P, 0, sizeof(uint32_t)*Key_size);
    memset(Q, 0, sizeof(uint32_t)*Key_size);
    
    hexify(seed, lenP, P);
    
    
    switch (fill_in(lenP, P)){
        case 0:    break;
        
        case 1:    printf("Seed not long enough -> patching by repeating it.\n");
                break;
        
        case 2:    printf("Seed is 0, can't generate key.\n");
                return 0;
        
    }
    
    gen_strong_prime(lenP, P);
    printf("Prime found.\n");
    memcpy(Q, P, sizeof(uint32_t)*(Key_size/2));
    
    while ( compare(lenQ-lenQ/2, &Q[lenQ/2], &P[lenQ/2]) == 1 ){
        /*    If P-Q is small the key can easily be factored, thus calculate
            new Q if they are too close.*/
        
        // Calculate a seed for Q from P (Needs to be somewhat heavy computationally, for security)
        for (int j=0; j<73; j++){
            new_int(lenQ, Q);
        }
        
        fill_in(lenQ, Q);
        
        printf("Generated prime seed\n");
        
        gen_strong_prime(Key_size/2, Q);
        printf("Prime found.\n");
        
        // If P has a non zero block more than Q, they are far enough apart.
        if( lenQ != lenP && P[lenP-1] != 0){
            break;
        }
    }
    
    primes_2_keys(pub_key_ptr, priv_key_ptr, lenP, P, lenQ, Q, 0, NULL);
    
    // Clear P and Q from memory - nice that you tried...
    memset(P, 0, (Key_size-Key_size/2)*sizeof(uint32_t));
    memset(Q, 0, (Key_size/2)*sizeof(uint32_t));
    
    return 0;
}

void encrypt_block(int lenMsg, uint32_t Msg[lenMsg], PublicKey * pub_key){
    exp_mod(lenMsg, Msg, pub_key->lenExp, pub_key->Exp, pub_key->PQ_mod);
}

void decrypt_block(int lenMsg, uint32_t Msg[lenMsg], PrivateKey * priv_key){
    
    uint32_t Msg2[2*(priv_key->lenP)], B[priv_key->lenQ];
    
    memcpy(Msg2, Msg, sizeof(Msg[0])*lenMsg);
    
    exp_mod(lenMsg, Msg2, priv_key->lenP, priv_key->Exp_p, priv_key->P_mod);
    exp_mod(lenMsg, Msg, priv_key->lenQ, priv_key->Exp_q, priv_key->Q_mod);
    
    if(compare(lenMsg, Msg2, Msg) == 0){
    
        if (priv_key->lenQ >= priv_key->lenP){
            memcpy(B, Msg, sizeof(Msg[0])*priv_key->lenQ);
            mod(priv_key->lenQ, B, priv_key->P_mod);
            add_array(priv_key->lenQ, Msg2, priv_key->P);
            sub_array(priv_key->lenQ, Msg2, B);
        }else{
            add_array(priv_key->lenP, Msg2, priv_key->P);
            sub_array(priv_key->lenP, Msg2, B);
        }
    }else{
        
        sub_array(lenMsg, Msg2, Msg);
    }
    
    
    mult_array(priv_key->lenP, Msg2, priv_key->lenP, priv_key->Q_inv, 2*(priv_key->lenP), Msg2);
    mod(2*(priv_key->lenP), Msg2, priv_key->P_mod);
    mult_array(priv_key->lenP, Msg2, priv_key->lenQ, priv_key->Q, lenMsg, Msg2);
    
    add_array( lenMsg, Msg, Msg2);
    mod(lenMsg,Msg, priv_key->PQ_mod);
}

int progress_bar_percentage = -1;

void progress_bar(int percentage){
    
    if (percentage == progress_bar_percentage){
        return;
    }
    
    // Contains 20 spaces to be filled in by "#"s
    char progress_str[21] = "                    ";
    
    for (int i = 0; i<percentage/5; i++){
        progress_str[i] = '#';
    }
    
    printf("\r[%20s]  (% 3d%%)", progress_str, percentage);
    
    if(percentage < 100){
        progress_bar_percentage = percentage;
    }else{
        printf("\n");
        progress_bar_percentage = -1;
    }
    
    fflush(stdout);
}

int file_exists(char * file_name){
    
    FILE *file = fopen(file_name, "r");
    
    if (file) {
        fclose(file);
        return 1;
    } else {
        return 0;
    }
}

int dont_replace_file(char *file_name){

    char response[100] = "0";
    
    if(file_exists(file_name)){
        while (1){
            printf("\nThe file '%s' already exists, do you want to override it? [yes/no]\n", file_name);
            if (
                /*     "%99s" causes the while loop to execute more than once if more than 99 characters are provided. 
                    Thus the otherwise unnecessarily large value of 99.*/
                scanf(" %99s", response)!=1
            ){
                response[0] = 0;
                response[1] = '\0';
            }
            
            if (response[0] == 'n' || response[0] == 'N' || response[0] == 'q' || response[0] == 'Q'){
                return 1;
            }else if(response[0] == 'y' || response[0] == 'Y'){
                return 0;
            }else{
                printf("Response not understood.\n");
            }
        }
    }
    
    return 0;
}

FILE *create_encrypt_output_file(char* file_name_in){
    
    int file_name_len = strlen(file_name_in);
    char file_name_out[file_name_len+10];
    FILE *file_out;
    
    
    strcpy(file_name_out, file_name_in);
    strcat(file_name_out, ".crypt");
    
    if (dont_replace_file(file_name_out)){
        return NULL;
        
    }else{
        file_out = fopen( file_name_out, "wb" );
        if (file_out == NULL){
            printf("Can't create '%s'. Stop.\n", file_name_out);
            return NULL;
        }
        
        printf("Encrypting  (%s -> %s).\n", file_name_in, file_name_out);
        return file_out;
    }
    
}

void encrypt_and_write_block(PublicKey *pub_key, 
                             FILE *file_out, 
                             uint32_t *data_buffer, 
                             uint32_t counter
){
        
    for(int i=0; i<key_length(pub_key); i++){
        data_buffer[i] ^= counter;
    }
    
    const uint32_t one = 1;
    
    memcpy(&((char*) data_buffer)[get_block_size(pub_key)],
           &one, 
           sizeof(uint32_t)*key_length(pub_key) - get_block_size(pub_key)
           );
    
    encrypt_block(key_length(pub_key), data_buffer, pub_key);
    
    fwrite(data_buffer, 1, key_length(pub_key)*sizeof(uint32_t), file_out);
    
}

int read_and_decrypt_block(PrivateKey *priv_key, 
                           FILE *file_in, 
                           uint32_t *data_buffer, 
                           uint32_t counter
){
        
    if(  
        fread(data_buffer, 1, sizeof(uint32_t)*key_length(priv_key), file_in)
         != sizeof(uint32_t)*key_length(priv_key) 
    ){
        printf("Failed to decrypt file.\n");
        return 1;
    }
    
    decrypt_block(key_length(priv_key), data_buffer, priv_key);
    
    for(int i=0; i<key_length(priv_key); i++){
        data_buffer[i] ^= counter;
    }
    
    return 0;
}

uint64_t encrypt_and_write_data(PublicKey *pub_key, 
                                FILE *file_in, 
                                FILE *file_out, 
                                uint32_t counter
){
    
    uint32_t data_buffer[key_length(pub_key)];
    uint64_t file_length = 0, delta = get_block_size(pub_key);
    
    fseek(file_out, key_length(pub_key)*sizeof(uint32_t), SEEK_SET);
    
    while( delta == get_block_size(pub_key) ){
        
        delta = fread(data_buffer, 1, get_block_size(pub_key), file_in);
        encrypt_and_write_block(pub_key, file_out, data_buffer, counter);
        file_length += delta;
        counter ++;
    }
    
    return file_length;
    
}

void write_crypt_header(PublicKey *pub_key, 
                        FILE *file_out, 
                        uint64_t file_length, 
                        uint32_t counter
){
    
    uint32_t header[key_length(pub_key)];
    
    memset(header, 0, sizeof(uint32_t)*key_length(pub_key));
    
    memcpy(header, &file_length, sizeof(uint64_t));
    
    header[2] = counter;
    header[3] = 0;
    
    fseek(file_out, 0, SEEK_SET);
    encrypt_and_write_block(pub_key, file_out, header, counter);
    
}

int read_crypt_header(PrivateKey *priv_key, FILE* file_in, uint64_t *file_length, uint32_t *counter){
    
    uint32_t header[key_length(priv_key)];
    
    if(
        read_and_decrypt_block(priv_key, file_in, header, 0)
    ){
        return 1;
    }
    
    memcpy(file_length, header, sizeof(uint64_t));
    
    *counter = header[3];
    *file_length = header[0] ^ (*counter);
    
    if (header[2] != 0){
        return 1;
    }
    
    return 0;
}

int read_and_decrypt_data(PrivateKey *priv_key, FILE *file_in, FILE *file_out){
    uint32_t data_buffer[key_length(priv_key)], counter;
    
    uint64_t file_length = 0;
    
    if (
        read_crypt_header(priv_key, file_in, &file_length, &counter)
    ){
        printf("Failed to read file header.\n");
        return 1;
    }
    
    while( file_length > get_block_size(priv_key) ){
        
        if(
            read_and_decrypt_block(priv_key, file_in, data_buffer, counter)
        ){
            return 1;
        }
        
        fwrite(data_buffer, 1, get_block_size(priv_key), file_out);
        
        file_length -= get_block_size(priv_key);
        counter ++;
    }
    
    if(
        read_and_decrypt_block(priv_key, file_in, data_buffer, counter)
    ){
        return 1;
    }
    
    fwrite(data_buffer, 1, file_length, file_out);
    
    return 0;
}

int encrypt_file(char* file_name_in, PublicKey * pub_key){
	
	FILE *file_in = fopen( file_name_in, "rb" );
    
    if (file_in == NULL){
        printf("Can't open '%s'. Stop.\n", file_name_in);
        return 1;
    }
	
	FILE *file_out = create_encrypt_output_file(file_name_in);
	
	if (file_out == NULL){ return 1; }
	
	uint32_t counter = 0x10101010 + clock();
	
	new_int(1, &counter);
	
	uint64_t file_length;
	
	file_length = encrypt_and_write_data(pub_key, file_in, file_out, counter);
	
	write_crypt_header(pub_key, file_out, file_length, counter);
	
    fclose(file_in);
    fclose(file_out);
    
	return 0;

}

char *decrypt_output_file_name(char* name_in, char* name_out){
    
    int name_len = strnlen(name_in, 200);
    
    if (name_len == 200){
        printf("File path is too long.\n");
        return NULL;
        
    }else if(name_len == 0){
        printf("Can't create '%s'. Stop\n", name_out);
        return NULL;
        
    }
    
    strcpy(name_out, name_in);
    
    char* dot_ptr = strchr( name_in, '.');
    
    if ( dot_ptr == NULL ){
        dot_ptr = strchr(name_in, '\0');
    }
    
    strcpy( &name_out[dot_ptr - name_in], "_out");
    strcpy( &name_out[dot_ptr - name_in] + 4, dot_ptr );
    
    name_len = strnlen(name_out, 200);
    
    if( strncmp(&name_out[name_len-7+1], ".crypt", 7) == 0 ){
        name_out[name_len-7+1] = '\0';
        
    }
    
    return name_out;
}

FILE * create_decrypt_output_file(char* name_in){
    
    char name_out[strnlen(name_in, 200)+4];
    
    decrypt_output_file_name(name_in, name_out);
    
    FILE *file_out;
    
    if (dont_replace_file(name_out)){
        printf("Can't create '%s'. Stop\n", name_out);
        return NULL;
        
    }else{
        
        file_out = fopen( name_out, "wb" );
        
        if (file_out == NULL){
            return NULL;
        }
    }
    
    printf("Decrypting (%s -> %s):\n", name_in, name_out);
    
    return file_out;
    
}

int decrypt_file(char* file_name_in, PrivateKey * priv_key){
    
    FILE *file_in = fopen( file_name_in, "rb" );
    
    if ( file_in == NULL ){
        printf("Can't open '%s'. Stop.\n", file_name_in);
        return 1;
    }
	
	FILE *file_out = create_decrypt_output_file(file_name_in);
	
	if (file_in == NULL){ return 1; }
	
	if(
	    read_and_decrypt_data(priv_key, file_in, file_out)
	){
	    printf("Could not decrypt '%s'.\n", file_name_in);
	}
    fclose(file_in);
    fclose(file_out);
    
	return 0;
    
}

int save_uint(FILE *key_file, const char * label, const int len, const uint32_t uint[len]){
    // Saves a uint32_t array to a file, does not open or close the file.
    fprintf(key_file, "%s:\n%d block(s)\n", label, len);
    for(int i=len-1; i>=0 ; i--){
        fprintf(key_file, "%08x ",uint[i]);
    }
    fprintf(key_file, "\n\n");
    return 0;
}

uint32_t * load_uint(FILE *key_file, int *len){
    
    char label[100];
    
    if( fscanf(key_file, "%10s:\n", label) == EOF ){
        printf("Key file ended unexpectedly(label)!\n");
        return NULL;
    };
    //printf("'%s'\n", label);
    if(fscanf(key_file, "%d block(s)\n", len) == EOF){
        printf("Key file ended unexpectedly(len)!\n");
        return NULL;
    };
    //printf("%d\n", *len);
    
    uint32_t *uint = malloc(sizeof(uint32_t)*(*len));
    
    for(int i=*len-1; i>=0 ; i--){
        if (fscanf(key_file, "%x ", &uint[i]) == EOF){
            printf("Key file ended unexpectedly(uint[%i])!\n", i);
            return NULL;
        };
    }
    
    if(fscanf(key_file, "\n\n") == EOF){
        printf("Key file ended unexpectedly(end)!\n");
        return NULL;
    };
    
    //printf("Done loading %s\n", label);
    return uint;
}

uint32_t save_priv_key(PrivateKey * priv_key, char* file_name){
    
    int file_name_len = strlen(file_name);
    char file_name_[file_name_len+100];
    memcpy(file_name_, file_name, sizeof(char)*(file_name_len) );
    memcpy(&file_name_[file_name_len], ".priv", sizeof(".priv"));
    
    
    if (dont_replace_file(file_name_)){
        return 2;
    }
    
    FILE *key_file = fopen( file_name_, "w" );
    if(strncmp(file_name, "temp", 5) != 0){
        // If this is a test run we only want to mention it if there is a problem.
        printf("Saving PrivateKey to '%s'.\n", file_name_);
    }
    
    save_uint(key_file, "P", priv_key->lenP, priv_key->P);
    save_uint(key_file, "Q", priv_key->lenQ, priv_key->Q);
    save_uint(key_file, "Q_inv", priv_key->lenP, priv_key->Q_inv);
    save_uint(key_file, "PQ", priv_key->PQ_mod.len, priv_key->PQ_mod.self);
    save_uint(key_file, "Exp_p", priv_key->lenP, priv_key->Exp_p);
    save_uint(key_file, "Exp_q", priv_key->lenQ, priv_key->Exp_q);
    
    fclose(key_file);
    return 0;
}

PrivateKey* load_priv_key(char* file_name){
    
    int file_name_len = strlen(file_name);
    char file_name_[file_name_len+100];
    memcpy(file_name_, file_name, sizeof(char)*(file_name_len) );
    memcpy(&file_name_[file_name_len], ".priv", sizeof(".priv"));
    
    FILE *key_file = fopen( file_name_, "r" );
    
    if(key_file == NULL){
        printf("Couldn't open '%s'.\n", file_name_);
        return NULL;
    }
    
    PrivateKey *priv_key = malloc(sizeof(PrivateKey));
    
    if(strncmp(file_name, "temp", 5) != 0){
        // If this is a test run we only want to mention it if there is a problem.
        printf("Loading PrivateKey from '%s'.\n", file_name_);
    }
    
    uint32_t *PQ;
    int lenPQ=-1;
    
    
    priv_key->P         = load_uint(key_file, &(priv_key->lenP));
    priv_key->Q         = load_uint(key_file, &(priv_key->lenQ));
    priv_key->Q_inv     = load_uint(key_file, &(priv_key->lenP));
    
    
    *(ModKit *) &priv_key->P_mod    = mod_prep_alloc(   priv_key->lenP,
                                                        priv_key->P
                                                        );
    
    *(ModKit *) &priv_key->Q_mod    = mod_prep_alloc(   priv_key->lenQ,
                                                        priv_key->Q
                                                        );
    
    PQ = load_uint(key_file, &lenPQ);
    *(ModKit *) &priv_key->PQ_mod    = mod_prep_alloc(   lenPQ,
                                                        PQ
                                                        );
    
    priv_key->Exp_p     = load_uint(key_file, &(priv_key->lenP));
    priv_key->Exp_q     = load_uint(key_file, &(priv_key->lenQ));
    
    fclose(key_file);
    
    return priv_key;
}

uint32_t save_pub_key(PublicKey * pub_key, char* file_name){
    
    int file_name_len = strlen(file_name);
    char file_name_[file_name_len+100];
    memcpy(file_name_, file_name, sizeof(char)*(file_name_len) );
    memcpy(&file_name_[file_name_len], ".pub", sizeof(".pub"));
    
    if (dont_replace_file(file_name_)){
        return 2;
    }
    
    FILE *key_file = fopen( file_name_, "w" );
    
    if(strncmp(file_name, "temp", 5) != 0){
        // If this is a test run we only want to mention it if there is a problem.
        printf("Saving PublicKey to '%s'.\n", file_name_);
    }
    
    save_uint(key_file, "PQ", pub_key->PQ_mod.len, pub_key->PQ_mod.self);
    save_uint(key_file, "Exp", pub_key->lenExp, pub_key->Exp);
    
    fclose(key_file);
    
    return 0;
}

PublicKey* load_pub_key(char* file_name){
    
    int file_name_len = strlen(file_name);
    char file_name_[file_name_len+100];
    memcpy(file_name_, file_name, sizeof(char)*(file_name_len) );
    memcpy(&file_name_[file_name_len], ".pub", sizeof(".pub"));
    
    
    FILE *key_file = fopen( file_name_, "r" );
    
    if(key_file == NULL){
        printf("Couldn't open '%s'.\n", file_name_);
        return NULL;
    }
    
    if(strncmp(file_name, "temp", 5) != 0){
        // If this is a test run we only want to mention it if there is a problem.
        printf("Loading PublicKey from '%s'.\n", file_name_);
    }
    
    PublicKey* pub_key = malloc(sizeof(PublicKey));
    
    uint32_t *PQ;
    int lenPQ;
    
    PQ = load_uint(key_file, &lenPQ);
    *(ModKit *) &pub_key->PQ_mod = mod_prep_alloc( lenPQ, PQ);
    pub_key->Exp = load_uint(key_file, &pub_key->lenExp);
    
    
    fclose(key_file);
    return pub_key;
}

void print_help(){
    
    printf(
        "Usage: crypt [options]\n"
        "Options:\n"
        "  --help               Display this message.\n"
        "  --keygen [seed]      Generate RSA key pair from a long *random* base 10 seed number.\n"
        "  --bits [value]       Specify key length in bits for --keygen.\n"
        "  --save *[file]       Save key to [file].\n"
        "  --load *[file]       Load key(s) in '[file].pub' and/or '[file].priv'.\n"
        "  --encrypt [file]     Encrypt [file] and save to '[file].crypt'.\n"
        "  --decrypt [file]     Decrypt [file] and save to '[file name]_out.[file extension]'.\n"
        "  --test *[value]      Run a test and timing routine.\n"
        "  --fulltest *[seed]   Run a comprehensive test routine.\n"
        "\n"
        "Inputs marked with '*' are optional.\n"
    );
    
    return;
}

int setup_default_keys(PublicKey **pub_key_ptr, PrivateKey **priv_key_ptr){
    
    int lenP = 32, lenQ = 32;
    uint32_t P[lenP], Q[lenQ];
        
    P[0]  = 0x8369cef9;
    P[1]  = 0x487be93a;
    P[2]  = 0x9ac0a738;
    P[3]  = 0x4d9ee47d;
    P[4]  = 0x3f2b452a;
    P[5]  = 0xd955d8d4;
    P[6]  = 0x269f457f;
    P[7]  = 0xdf97540;
    P[8]  = 0x5a831a3b;
    P[9]  = 0x3927f8f3;
    P[10] = 0x28b7c4e8;
    P[11] = 0x5f74ab69;
    P[12] = 0x1eb8db19;
    P[13] = 0xb3bf94f;
    P[14] = 0x692c4adb;
    P[15] = 0xa1a3bc90;
    P[16] = 0x82e121a9;
    P[17] = 0xe428e1fd;
    P[18] = 0xf445baf8;
    P[19] = 0x2cd1e16b;
    P[20] = 0x6422eb47;
    P[21] = 0x9cb45866;
    P[22] = 0x3e780327;
    P[23] = 0x17a8c4a2;
    P[24] = 0xfefa152c;
    P[25] = 0x177c2b79;
    P[26] = 0x15b7c24d;
    P[27] = 0x7b0a2c36;
    P[28] = 0x5b4703f4;
    P[29] = 0x4269277d;
    P[30] = 0x2ec0bd1a;
    P[31] = 0x3cc7fd56;
    
    Q[0]  = 0x1f18f777;
    Q[1]  = 0x9f7d2d5d;
    Q[2]  = 0x83220202;
    Q[3]  = 0xda610918;
    Q[4]  = 0x6b2869f2;
    Q[5]  = 0xd9f66e3b;
    Q[6]  = 0xe1ad77c1;
    Q[7]  = 0x5486da3d;
    Q[8]  = 0x306ee2b8;
    Q[9]  = 0x211e10a9;
    Q[10] = 0xebdf5fd1;
    Q[11] = 0x243e1af5;
    Q[12] = 0xc883a01d;
    Q[13] = 0x92fe6b9c;
    Q[14] = 0x63f257fe;
    Q[15] = 0x7bbf889a;
    Q[16] = 0x199820f2;
    Q[17] = 0xb8c95113;
    Q[18] = 0x273067;
    Q[19] = 0xb81a6d11;
    Q[20] = 0xc8c9e845;
    Q[21] = 0xb3af934e;
    Q[22] = 0xe7188e88;
    Q[23] = 0x167bdabb;
    Q[24] = 0x43a1090d;
    Q[25] = 0xa548b62a;
    Q[26] = 0x41ad6759;
    Q[27] = 0x9e828f24;
    Q[28] = 0x53ab3ac5;
    Q[29] = 0xc0193801;
    Q[30] = 0xb7d746cb;
    Q[31] = 0x7;
    
    return primes_2_keys(pub_key_ptr, priv_key_ptr, lenP, P, lenQ, Q, 0, NULL);
}

#define LOAD_KEY        0x00000001
#define GENERATE_KEY    0x00000002
#define DEFAULT_KEY     0x00000008

#define SAVE_KEY        0x00000010

#define ENCRYPT_FILE    0x00000100
#define DECRYPT_FILE    0x00000200

#define TEST_MODE       0x00001000

struct Options {
    uint32_t flags;
    int test_repeats,
        Key_size; // Key size (in blocks of uint32_t's)
    char *seed,
         *key_file,
         *encrypt_file_name,
         *decrypt_file_name;

};

struct Options default_options(){
    struct Options options = {  0, 
                                200, 
                                2048/sizeof(uint32_t)/8, 
                                "20349578320495783405978", 
                                "key_file", 
                                NULL, 
                                NULL
                            };
    return options;
}

struct Options read_options(int argc, char ** argv){

    struct Options options = default_options();
    
    for (int i=1; i< argc; i++){
        if (strncmp(argv[i], "-", 1) == 0){
            if(strncmp(argv[i], "--keygen", 9) == 0){
                
                if (argc > i+1  && strncmp(argv[i+1], "-", 1) != 0){
                    options.flags |= GENERATE_KEY;
                    options.seed = argv[i+1];
                    i++;
                }else{
                    printf("No seed argument was passed to '--keygen', please specify a long *random* base 10 number.\n");
                    return default_options();
                }
                
            }else if(strncmp(argv[i], "--bits", 7) == 0){
            
                if (argc > i+1  && strncmp(argv[i+1], "-", 1) != 0){
                    options.Key_size = atoi(argv[i+1])/sizeof(uint32_t)/8;
                    i++;
                }else{
                    printf("No bit length argument was passed after '--bits'. Stop.\n");
                    return default_options();
                }
                
            }else if(strncmp(argv[i], "--help", 7) == 0){
                print_help();
                return default_options();
                
            }else if(strncmp(argv[i], "--load", 7) == 0){
                options.flags |= LOAD_KEY;
                if (argc > i+1 && strncmp(argv[i+1], "-", 1) != 0){
                    options.key_file = argv[i+1];
                    i++;
                }
            
            }else if(strncmp(argv[i], "--save", 7) == 0){
                options.flags |= SAVE_KEY;
                if (argc > i+1 && strncmp(argv[i+1], "-", 1) != 0){
                    options.key_file = argv[i+1];
                    i++;
                }
                
            }else if(strncmp(argv[i], "--encrypt", 10) == 0){
                if (argc > i+1  && strncmp(argv[i+1], "-", 1) != 0){
                    options.flags |= ENCRYPT_FILE;
                    options.encrypt_file_name = argv[i+1];
                    i++;
                }else{
                    printf("No file name argument was passed after '--encrypt'. Stop.\n");
                    return default_options();
                }
                
            }else if(strncmp(argv[i], "--decrypt", 10) == 0){
                if (argc > i+1  && strncmp(argv[i+1], "-", 1) != 0){
                    options.flags |= DECRYPT_FILE;
                    options.decrypt_file_name = argv[i+1];
                    i++;
                }else{
                    printf("Fatal: No file name argument was passed after '--decrypt'. Stop.\n");
                    return default_options();
                }
                
            }else if(strncmp(argv[i], "--test", 7) == 0){
                options.flags |= TEST_MODE;
                if (argc > i+1 && strncmp(argv[i+1], "-", 1) != 0){
                    options.test_repeats = atoi( argv[i+1] );
                    i++;
                }
                
            }else if(strncmp(argv[i], "--fulltest", 11) == 0){
                
                options.flags |= GENERATE_KEY | TEST_MODE | ENCRYPT_FILE | DECRYPT_FILE;
                options.decrypt_file_name = "RSA.svg.crypt";
                options.encrypt_file_name = "RSA.svg";
                
                if (argc > i+1 && strncmp(argv[i+1], "-", 1) != 0){
                    options.seed = argv[i+1];
                    i++;
                }
                
                printf(
                    "Running full test\n"
                    "(Remember to check the integrity of out_RSA.svg afterwards):\n"
                    "Testing help message:\n"
                    "============================\n"
                );
                print_help();
                printf("============================\n");
                
            }else{
                printf("Unknown option '%s'.\n\n", argv[i]);
                print_help();
                return default_options();
            }
        }else{
            printf("Unknown option '%s'.\n\n", argv[i]);
            print_help();
            return default_options();
        }
    }
    
    return options;
}

int main(int argc, char **argv){
    
    if ( argc == 1) {
        print_help();
        return 0;
    }
    
    struct Options options = read_options(argc, argv);
    
    // Setting up the keys
    PrivateKey *priv_key = NULL;
    PublicKey *pub_key = NULL;
    
    if(options.flags & GENERATE_KEY){
        
        printf("Generating Key:\n");
        if ( !(options.flags & SAVE_KEY) ){
            printf(
                "\nWarning: You are generating a key pair, but not saving it.\n"
                "\tAdd the option ' --save file_name', to save the keys to 'filename.priv' and 'filename.pub'.\n\n"
            );
            
        }
        keygen(options.Key_size, &pub_key, &priv_key, options.seed);
        
    }else if(options.flags & LOAD_KEY){
        // Load key
        pub_key = load_pub_key(options.key_file);
        priv_key = load_priv_key(options.key_file);
        
    }else{
        
        // Use default key (N.b. it's an edge case (short key) in decrypt/encrypt, because (P*Q)[32*2-1] := 1.)
        setup_default_keys(&pub_key, &priv_key);
        
    }
    
    if (options.flags & SAVE_KEY){
        save_priv_key(priv_key, options.key_file);
        save_pub_key(pub_key, options.key_file);
    }
    
    if ((options.flags & TEST_MODE) && priv_key && pub_key){
        test_routine(pub_key, priv_key, options.test_repeats);
    }
    
    if ((options.flags & ENCRYPT_FILE) && pub_key != NULL){
        encrypt_file(options.encrypt_file_name, pub_key);
    }
    
    if ((options.flags & DECRYPT_FILE) && priv_key != NULL){
        decrypt_file(options.decrypt_file_name, priv_key);
    }
    
    if (priv_key){
        free_PrivateKey(priv_key);
    }
    
    if(pub_key){
        free_PublicKey(pub_key);
    }
    
    return 0;
}
