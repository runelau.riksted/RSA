#ifndef crypt
#define crypt
#include <stdint.h>

#include <stdio.h>
#include "uint_arithmetic.h"

typedef struct PrivateKey{
	int lenP;				// lenght of P
	int lenQ;				// lenght of Q
	uint32_t * P;			// First Prime
	uint32_t * Q;			// Second Prime
	uint32_t * Q_inv;		// The inverse of Q in Z/PZ i.e. Q_inv*Q = 1 (mod P)
	const ModKit P_mod;		// ModKit for P
	const ModKit Q_mod;		// ModKit for Q
	const ModKit PQ_mod;	// ModKit for P*Q
	uint32_t * Exp_p;		// Private exponent modulo P
	uint32_t * Exp_q;		// Private exponent modulo Q
	
}PrivateKey;

void free_clear_uint32_t_array(int lenA, const uint32_t A[lenA]);

void free_PrivateKey(PrivateKey * key);

typedef struct PublicKey{
	const ModKit PQ_mod;	// ModKit for the modulus (P*Q)
	uint32_t * Exp;			// Public exponent
	int lenExp;				// lenExp length of Exp
	
}PublicKey;


void free_PublicKey(PublicKey * key);

uint32_t mod_30(int lenP, uint32_t P[lenP]);

uint32_t gen_prime(int lenP, uint32_t P[lenP]);

void gen_strong_prime_candidate(int lenQ, uint32_t nxqQ[3][lenQ*2+2], int lenP, uint32_t P[lenP]);

uint32_t gen_strong_prime(int lenP, uint32_t P[lenP]);

uint32_t primes_2_keys( PublicKey ** pub_key_ptr, PrivateKey ** priv_key_ptr, int lenP, uint32_t *P_in, int lenQ, uint32_t *Q_in, int lenPubExp, uint32_t * PubExp);

uint32_t fill_in(uint32_t lenA, uint32_t * A);

uint32_t keygen( int M, PublicKey ** pub_key_ptr, PrivateKey ** priv_key_ptr,  char* seed);

void encrypt_block(int M, uint32_t Msg[M], PublicKey * pub_key);

void decrypt_block(int M, uint32_t Msg[M], PrivateKey * priv_key);

void progress_bar(int percentage);

int file_exists(char * file_name);

int dont_replace_file(char *file_name);

int encrypt(char* file_name_in, PublicKey * pub_key);

int gen_output_name(char* name_in, char* name_out);

int decrypt(char* file_name_in, PrivateKey * priv_key);

int save_uint(FILE *key_file, const char * label, const int len, const uint32_t uint[len]);

uint32_t * load_uint(FILE *key_file, int *len);

uint32_t save_priv_key(PrivateKey * priv_key, char* file_name);

uint32_t save_pub_key(PublicKey * pub_key, char* file_name);

PublicKey* load_pub_key(char* file_name);

PrivateKey* load_priv_key(char* file_name);

void print_help();

#endif
