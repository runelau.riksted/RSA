#include <string.h>
#include <stdio.h>
#include "uint_arithmetic.h"

int test_left_shift_array(){

    uint32_t I[10], J[10];
    
    memset(I, 0, sizeof(*I)*10);
    
    I[7] = ~0;
    I[3] = 0x42048aaf;
    I[2] = 0x24cb16dd;
    I[1] = 0x57a3fff9;
    I[0] = 0x0000ffff;
    
    
    left_shift_array(1, I, 0, 1);
    if (I[0] != 0x0000ffff<<1){
        printf("'left_shift_array' fails when lenI = 1:\n 0x0000ffff<<1 -> %x.\n", I[0]);
        return 1;
    }
    
    memcpy(J, I, sizeof(*I)*10);
    
    left_shift_array(10, I, 0, 0);
    
    if (compare(10, I, J) != 1){
        printf("'left_shift_array' by 0 changes the input:\n");
        printv(10, J);
        printf("->\n");
        printv(10, I);
        return 1;
    }
    
    memcpy(I, J, sizeof(*I)*10);
    
    left_shift_array(10, I, 0, 7);
    left_shift_array(10, I, 1, 25);
    
    if (compare(8, &I[2], J) != 1 || I[0] != 0 && I[1] != 0){
        printf("'left_shift_array' by 64 didn't work:\n");
        printv(10, J);
        printf("->\n");
        printv(10, I);
        return 1;
    }
    return 0;
}

int main(){
    test_left_shift_array();
}






