#ifndef test_crypt
#define test_crypt
#include "crypt.h"
#include <stdio.h>
#include "uint_arithmetic.h"
#include "crypt.h"

int test_raw_key(PublicKey* pub_key, int lenPQ, uint32_t *priv_exp);

int is_prime_candidate_strong(int lenP, uint32_t *P, int lenQ, uint32_t *Q, uint32_t *q);

int test_routine(PublicKey *pub_key, PrivateKey *priv_key, int test_repeats);
#endif
