/*This file is #includ'ed twice in uint_arithmetic.c. Depending on the value of mod0_div1
 it defines modulus or division. These two algorithms are essentially the same except division keeps
 track of the number of times the modulus was subtracted, thus it is better to keep them together.*/

#define check_new_mod_div_result 0
#if mod0_div1 == 0
uint32_t mod_coarse(int lenA, uint32_t *A, const ModKit MOD ){
//A := A%MOD.self, The ModKit, should be the return value of mod_prep
	
	uint32_t mult_res[MOD.len+modn];
	
	lenA = sig_len(lenA, A);
	
	const int N = MOD.len + modn;
	
	if (lenA > N){
	
		int k = ((lenA-N-1)/modn)*modn;
		
		if(k + N != lenA){
			mult_array(MOD.len, MOD.mod2n, lenA -k - N, &A[k+N], lenA -k -modn, mult_res);

			if(
				add_array(lenA -k -modn, &A[k], mult_res)
			){
				//capture overflow
				A[lenA - modn] ++;
			}
		}
		
		for(k -= modn; k >= 0; k -= modn) {
			
			mult_array(MOD.len, MOD.mod2n, modn, &A[k+N], N, mult_res);

			if(
				add_array(N, &A[k], mult_res) 
			){
				//Overflow occurred, the overlow is congruent to MOD.mod2n.
				A[k+MOD.len] += add_array(MOD.len, &A[k], MOD.mod2n);
			}
		}
		
		memset( &A[N], 0, sizeof(A[0])*(lenA - N) );
		return MOD.len + modn;
		
	}

	return lenA;

}
#endif


#if mod0_div1 == 0
uint32_t mod_fine(int lenA, uint32_t *A, const ModKit MOD ){
#else
uint32_t div_array(int lenA, uint32_t *A, const ModKit MOD, int lenD, uint32_t *D ){
#endif
	lenA = sig_len(lenA, A);
	
	#if check_new_mod_div_result
		int a0 = A[lenA-1];
	#endif
	
	
	#if mod0_div1
		uint32_t initial_subtraction = 0;
		memset(D, 0, sizeof(D[0])*lenD);
		if (lenD < lenA-MOD.len){
			printf("D not long enough to contain divisor\n");
		}
	#endif
	
	if(MOD.len>lenA){
		return 0;
	}
	
	if(compare(MOD.len, &A[lenA-MOD.len], MOD.shift)>0){
		sub_array(MOD.len , &A[lenA-MOD.len], MOD.shift);
		
		#if mod0_div1
			// We need to remember this subtraction and add it to D later.
			initial_subtraction = 1;
		#endif
	}
	
	uint64_t ratio = 0;
	uint32_t MOD_mult[MOD.len+1];
	
	if(MOD.len<lenA){
		for (int k=lenA-MOD.len-1; k>=0; k--){
			ratio = (uint64_t)(*(uint64_t *) &A[MOD.len-1 + k]) / MOD.shift[MOD.len -1];
			
			#if check_new_mod_div_result
				if (ratio> (uint64_t) ((uint32_t)~0) + MOD.uncer){
					printf("Ratio > uint32_t, ratio = %lx\n", ratio);
				}
			#endif
			
			if(ratio >(uint32_t) ~0){
				ratio = (uint32_t) ~0;
			}
			
			#if mod0_div1
				if(k<lenD){
					D[k] = (uint32_t) ratio;
				}
			#endif
			
			mult_array(MOD.len, MOD.shift, 1, (uint32_t*) &ratio, MOD.len+1, MOD_mult);
			for(int i = 0; i<MOD.uncer; i++){
				if(compare(MOD.len +1, &A[k], MOD_mult)==0){
					if(
						sub_array(MOD.len, MOD_mult, MOD.shift)
					){
						MOD_mult[MOD.len]--;
					
					}
					#if mod0_div1
					if(k<lenD){
						D[k] --;
					}
					#endif
					#if check_new_mod_div_result
						ratio --;
					#endif
				}else{
					break;
				}
			}	
			
			sub_array(MOD.len +1 , &A[k], MOD_mult);
			
			#if check_new_mod_div_result
				if (A[k+MOD.len] || compare(MOD.len, &A[k], MOD.shift )>0){
					printf("A>MOD.shift!!!*2^k, ratio= %lx\n", ratio);
				}
			#endif
		}
	}
	
	if(MOD.pow !=0){
		#if mod0_div1
			if(
				D[lenD-1] >> (32 - MOD.pow)
			){
				printf("D not long enough to contain divisor\n");
			}
			left_shift_array(lenD, D, 0, MOD.pow);
		#endif
		
		if(MOD.len>=2){
			ratio = (uint64_t)(*(uint64_t *) &A[MOD.len-2]) / MOD.shift[MOD.len -1];
		}else{
			/*Does the same as the above, but assumes A[-1] =0*/
			ratio = 0;
			memcpy(((uint32_t *) &ratio +1), &A[0], sizeof(uint32_t));
			ratio = (uint64_t) ratio / MOD.shift[0];
		}
		
		#if check_new_mod_div_result
			if (ratio> (uint64_t) ((uint32_t)~0) + MOD.uncer){
				printf("Ratio > uint32_t, ratio = %lx\n", ratio);
			}
		#endif
		
		if(ratio >(uint32_t) ~0){
			ratio = (uint32_t) ~0;
		}
		
		ratio = ratio&((~0)<<(sizeof(uint32_t)*8-MOD.pow));
		mult_array(MOD.len, MOD.shift, 1, (uint32_t *) &ratio, MOD.len+1, MOD_mult);
		
		#if mod0_div1
			D[0] += ratio>>(sizeof(uint32_t)*8-MOD.pow);
		#endif
		
		for(int i = 0; i<MOD.uncer; i++){
			if(compare(MOD.len, A, &MOD_mult[1])==0){
				if(sub_array(MOD.len, &MOD_mult[1], MOD.self)){
					printf("Underflow!!!\n");// gcc -O3 breaks the code, if this print statement is removed.
				}
				
				#if mod0_div1
					D[0] --;
				#endif
				#if check_new_mod_div_result
					ratio --;
				#endif

			}else{
				break;
			}
		}
		
		sub_array(MOD.len , A, &MOD_mult[1]);
		#if check_new_mod_div_result == 2
			printf("Mask=%x\t", (~0)<<(sizeof(uint32_t)*8-MOD.pow));
			printf("A - MOD.shift*%lx:\n\t", ratio);
			printv(lenA, A);
		#endif
	}
	
	#if mod0_div1
		if(initial_subtraction){
			D[lenA-MOD.len] += 1<<MOD.pow;
		}
	#endif
	
	#if check_new_mod_div_result
		if( sig_len(lenA- MOD.len, &A[MOD.len]) ){
			printf("MOD.len=%d\tlenA=%d\tA[lenA-1]=%u\t\t smaller=2-c1 A[%d]=%x\n", MOD.len, lenA, a0, i, A[i]);
			return 1;
		}
		
		if(compare(MOD.len, A, MOD.self)>0){
			printf("MOD.len=%d\tlenA=%d\tA[lenA-1]=%x\t\t smaller=%dc2\n", MOD.len, lenA, a0,compare(MOD.len, A, MOD.self ));
			return 1;
		}
	#endif
	return 0;
	
}

//mod_division.c defines the mod or division function depending of the value of mod0_div1
#if mod0_div1 == 0
uint32_t mod(int lenA, uint32_t *A, ModKit MOD ){
	//A := A%MOD.self, The ModKit, should be the return value of mod_prep
	
	lenA = mod_coarse(lenA, A, MOD );
	return mod_fine( lenA, A, MOD);

}
#endif
