#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "uint_arithmetic.h"
#include <stdint.h>

#ifndef MIN
#define MIN(X,Y) ( (X) < (Y) ? (X) : (Y) )
#endif
#ifndef MAX
#define MAX(X,Y) ( (X) > (Y) ? (X) : (Y) )
#endif
uint32_t printv(const int lenv, const uint32_t v[lenv]){
	printf("[");
		for(int i = 0; i < lenv; i++){
			printf("%#10x\t", v[i]);
		}
	printf("]\n");
	return 0;
}

inline uint32_t sig_len(const int lenA, const uint32_t *A){
	for (int n=lenA-1; n>=0; n--){
		if (A[n] != 0){
			return n+1;
		}
	}
	
	return 0;
}

uint32_t compare_var(const int lenA, const uint32_t * const A, const int lenB, const uint32_t * const B){
	/*return 0 if A<B, 1 if A=B, 2 if A>B, */
	
	if(lenA == lenB){
		//pass
	
	}else if(lenA > lenB){
		for(int n=lenA-1; n>=lenB; n--){
			if(A[n] > 0 ){
				return 2;
			}
		}
		
	}else{ // lenA < lenB
		for(int n=lenB-1; n>=lenA; n--){
			if(0 < B[n] ){
				return 0;
			}
		}
	}
	
	for (int n = MIN(lenA, lenB)-1; n >= 0; n--){
		
		if (A[n] < B[n]){
			return 0;
			
		}else if (A[n] > B[n]){
			return 2;
			
		}
		
	}

	return 1;
}

uint32_t compare(const int len, const uint32_t * const A, const uint32_t * const B){
	/*return 0 if A<B, 1 if A=B, 2 if A>B, */
		
	for (int n=len-1; n>=0; n--){
		
		if (A[n] < B[n]){
			return 0;
			
		}else if (A[n] > B[n]){
			return 2;
			
		}
	}

	return 1;
}

inline void right_shift_array(uint32_t lenI, uint32_t *I, int d, int r){
    
    lenI = sig_len(lenI, I);
    
    if (lenI-d <= 0){
        memset(I, 0, sizeof(I[0])*lenI);
        return;
    }
    
    if(r > 0){
        for(int i=0; i+1+d<lenI; i++){
		    I[i] = ( I[i+d] >> r ) | ( I[i+1+d] << (32-r) );
	    }
	    
	    I[lenI-d-1] = I[lenI-1] >> r;
    }
	
	if (d){
		memset(&I[lenI-d], 0, d*sizeof(I[0]));
	}
}

inline void left_shift_array(uint32_t lenI, uint32_t *I, int d, int r){

    if(d > 0){
        memmove(&I[d], I, sizeof(*I)*(lenI-d));
        memset(I, 0, sizeof(*I)*d);
        I = &I[d];
        lenI -= d;
    }
    
    uint64_t temp = 0;
    if (r > 0){
        temp = I[lenI-1]<<r;
        I[lenI-1] = (uint32_t) temp;
        for(int i=lenI-2; i>=0; i--){
            temp <<= 32;
            temp += ((uint64_t) I[i])<<r;
	        *((uint64_t*) &I[i]) = temp;
        }
    }
}
    
inline uint32_t mult2n(const uint32_t lenI, uint32_t * const I, int n){
	// Multiply the uint32_teger represented by array I by 2^n
	#define verbose_mult2n  0
	int r;
	int d;
	#if verbose_mult2n
		printf("multiplying by 2^%d\n", n);
	#endif
	
	if(n > 0){
		r= n%32;
		d= n/32;
		
		int sig_lenI = sig_len(lenI, I);
		
		if( sig_lenI + d  < lenI ){
		    
		    left_shift_array(sig_lenI + d +1, I, d, r);
		    return 0;
		
		}else if ( sig_lenI + d == lenI  && I[sig_lenI-1] >> (32-r) == 0){
		
		    left_shift_array(lenI, I, d, r);
		    return 0;
		    
		}else{
		
		    left_shift_array(lenI, I, d, r);
		    return 1;
		    
		}
		
	}if( n < 0 ){
		
		n = -n;
		r= n%32;
		d= n/32;
		
		right_shift_array(lenI, I, d, r);
		
		return 0;
		
	}
	
	return 0;
}

uint32_t add_array_var(const int lenA, uint32_t * const A, const int lenB, const uint32_t * const B){
	/*calculate A:= A + B*/
	uint64_t rem = 0;
	
	const uint64_t maxi = ~0;
	uint64_t sum = 0;
	uint64_t* const a = (uint64_t*) A;
	uint64_t* const b = (uint64_t*) B;
	uint64_t bn;
	int n;
	
	#if 0 
	if(lenA>lenB){
		return (uint32_t) maxi;
	}
	#endif
	// a and b represent their integers by arrays of uint64_ts i.e. there are half as many entries 
	for (n=0; n<lenB/2; n++){
		bn = b[n];
		sum = a[n] + bn;
		if(bn>sum){
			/*overflow has occurred*/
			a[n] = sum + rem;
			rem = 1;
			
		}else if(sum == maxi && rem == 1){
			/*overflow will occur with sum + rem*/
			a[n] = 0;
			// rem = 1;
		}else{
			/*smaller than maxi*/
			a[n] = sum + rem;
			rem = 0;
		}
	}
	
	// If A and B are represented by an uneven amount of uint32_ts, then we need to take care of the last entry separately
	if( (lenB&1)==1 ){
			
			sum = (uint64_t) A[lenB-1] + B[lenB-1] + rem;
			A[lenB-1] = (uint32_t) sum;
			return sum>A[lenB-1];
	}
	
	n = lenB;
	while (rem == 1 && n<lenA){
		A[n] += 1;
		if(A[n] != 0){
			//no overflow
			return 0;
		}
		n++;
	}
	
	return rem;
}

uint32_t add_array(const int len, uint32_t * const A, const uint32_t * const B){
	/*calculate A:= A + B*/
	uint64_t rem = 0;
	
	const uint64_t maxi = ~0;
	uint64_t sum = 0;
	uint64_t* a = (uint64_t*) A;
	uint64_t* b = (uint64_t*) B;
	uint64_t bn;
	
	// a and b represent their integers by arrays of uint64_ts i.e. there are half as many entries 
	for (int n=0; n<len/2; n++){
		bn = b[n];
		sum = a[n] + bn;
		if(bn>sum){
			/*overflow has occurred*/
			a[n] = sum + rem;
			rem = 1;
			
		}else if(sum == maxi && rem == 1){
			/*overflow will occur with sum + rem*/
			a[n] = 0;
			// rem = 1;
		}else{
			/*smaller than maxi*/
			a[n] = sum + rem;
			rem = 0;
		}
	}
	
	// If A and B are represented by an uneven amount of uint32_ts, then we need to take care of the last entry separately
	if( (len&1)==1 ){
		
		sum = (uint64_t) A[len-1] + B[len-1] + rem;
		A[len-1] = (uint32_t) sum;
		rem = sum>A[len-1];
	}
	
	return rem;
}

uint32_t sub_array(const int len, uint32_t *A, const uint32_t *B){
	/* calculates A := A - B*/
	uint64_t rem = 0;
	uint64_t* a = (uint64_t*) A;
	uint64_t* b = (uint64_t*) B;
	
	// a and b represent their integers by arrays of uint64_ts i.e. there are half as many entries 
	for (int n=0; n<len/2; n++){
		
		if(a[n]< b[n]|| (a[n] == b[n] && rem == 1)){
			a[n] -= b[n] + rem;
			rem = 1;  
		}else{
			a[n] -= b[n] + rem;
			rem = 0;
		}   
	}
	
	// If A and B are represented by an uneven amount of uint32_ts, then we need to take care of the last entry separately
	if( (len&1)==1 ){
		if(A[len-1] < B[len-1] || (A[len-1] == B[len-1] && rem == 1)){
			A[len-1] -= B[len-1] + rem;
			rem = 1;  
		}else{
			A[len-1] -= B[len-1] + rem;
			rem = 0;
		}
	}
	return rem;
}

uint32_t mult_array(const int lenA, const uint32_t * const A, const int lenB, const uint32_t * const B, const uint32_t lenC, uint32_t * const C){
	// C:= A*B
	
	uint32_t res[lenA+lenB];
	uint64_t temp =0;
	/*uint32_t temp_a[2] = {0,0};*/
	uint32_t * const temp_pnt = (uint32_t *) &temp;
	
	memset(res, 0, (lenA+lenB)*sizeof(res[0]));
	
	for( int i = 0; i<lenA; i++){
		temp = 0;
		for(int j=0; j<lenB; j++){
			
			temp = (uint64_t) A[i]*B[j] + res[i+j] + temp_pnt[1];
			res[i+j] = temp_pnt[0];
			
		}
		
		res[i+lenB] = temp_pnt[1];
	}
	
	
	if (lenC >= lenA+lenB){
		/*save whole result in C*/
		memcpy(C, res, (lenA+lenB)*sizeof(A[0]));
		
	}else{
		/*save first part of result in C, discard the rest.*/
		
		memcpy(C, res, lenC*sizeof(A[0]));
		return res[lenC];
	}
	
	return 0;
}

// Definition of mod
#define mod0_div1 0
#include "mod_division.c"

#undef mod0_div1

// Definition of division
#define mod0_div1 1
#include "mod_division.c"

ModKit mod_prep(int lenMOD,  const uint32_t * const MOD, uint32_t * const MOD_Data){
	#define verbose_mod_prep 0
	
	#if verbose_mod_prep
	printf("MOD=\t\t");
	printv(lenMOD,MOD);
	#endif
	
	
	memcpy(MOD_Data, MOD, lenMOD*sizeof(MOD[0]));
	// Set lenMOD such that lenMOD-1 is the highest index where MOD[lenMOD-1] !=0.
	lenMOD = sig_len(lenMOD, MOD);
	
	const uint32_t half = 1<<(32-1);
	
	int pow = -1;
	// Set kit.pow to the number of leading zeros in MOD[lenMOD-1]
	for( int i = 0; i<32; i++){
		if((half>>i)&MOD[lenMOD-1]){
			pow = i;
			break;
		}
	}
	
	if(pow == -1){
		printf("Prepering ModKit, where MOD=0\n");
	}
	
	// Set MOD_shift to MOD*2^(kit.pow) i.e shift MOD to the left such that there are no leading zero bits in MOD_SHIFT[lenMOD-1].
	
	left_shift_array(lenMOD, MOD_Data, 0, pow);
    
	int uncer= 2;
	// Check leading bit of MOD[lenMOD-2]
	if((half&MOD_Data[lenMOD-2])==0){
		uncer = 1;
		
	}
	
	ModKit kit = {lenMOD, pow, uncer,  MOD, MOD_Data, &MOD_Data[lenMOD]};
	
	uint32_t One_len = lenMOD + modn + 1, One[One_len];
	memset(One, 0, sizeof(One[0])*(One_len));
	
	One[One_len-1] = 1;
	
	mod_fine(One_len, One, kit);
	memcpy(&MOD_Data[lenMOD], One, sizeof(One[0])*kit.len );
	
	
	#if verbose_mod_prep
	printf("MOD.len=%d,\tMOD.pow=%d,\tMOD.uncer=%d\n", kit.len, kit.pow, kit.uncer);
	printf("MOD.self=\t");
	printv(lenMOD,kit.self);
	printf("MOD.shift=\t");
	printv(lenMOD,kit.shift);
	#endif
	
	return kit;
	
}

ModKit mod_prep_alloc(const int lenMOD, const uint32_t *MOD){
	return mod_prep(lenMOD, MOD, malloc(sizeof(uint32_t)*lenMOD*2));
}

uint32_t exp_mod(int lenB, uint32_t *B, int lenE, const uint32_t * const E, const ModKit MOD){
	#define verbose_exp_mod 0
	
	#if verbose_exp_mod
		printf("exp_mod_main.\nMod=\n");
		printv(MOD.len, MOD.self); 
	#endif
	
	uint32_t Bsq[2*MOD.len + 2*modn];
	uint32_t res[2*MOD.len + 2*modn];
	
	lenE = sig_len(lenE, E);
	
	if (lenB >= MOD.len){
		mod(lenB, B, MOD);
		lenB = MOD.len;
	}
	
	memset(Bsq, 0, (MOD.len+modn)*sizeof(Bsq[0]));
	memcpy(Bsq,B, lenB*sizeof(Bsq[0]));
	
	memset(res, 0, 2*(MOD.len+modn)*sizeof(res[0]));
	res[0] = 1;
	#if verbose_exp_mod
		printf("Bsq=\n");
		printv(2*(MOD.len+modn), Bsq);
		printf("res=\n");
		printv(2*(MOD.len+modn), res);
	#endif
		
	
	const uint32_t half = 1<<(sizeof(uint32_t)*8-1);
	uint32_t relevant_bits = sizeof(Bsq[0])*8; 
	
	for (int i=0; i<lenE; i++){
		if( i == lenE-1){
			// For the leading block of the exponent it isn't necessary to iterate over the leading 0 bits. 
			for( int k = 0; k<sizeof(uint32_t)*8; k++){
				if((half>>k)&E[lenE-1]){
					relevant_bits -= k;
					break;
				}
			}
			//printf("relevant_bits = %d\n", relevant_bits);
		}
		for (int j=0; j<relevant_bits; j++){
			if(i !=0||j!=0){
				
				mult_array((MOD.len+modn), Bsq, (MOD.len+modn), Bsq, 2*(MOD.len+modn), Bsq);
				#if verbose_exp_mod == 3
					printf("i=%d, j=%d\nBsq=\n", i, j);
					printv(2*(MOD.len+modn), Bsq);
				#endif
				mod_coarse(2*(MOD.len+modn), Bsq, MOD);
				
			}
			
			if( ((1 << j)&E[i]) !=0){
				mult_array((MOD.len+modn), res, (MOD.len+modn), Bsq, 2*(MOD.len+modn), res);
				mod_coarse(2*(MOD.len+modn), res, MOD);
			}
			#if verbose_exp_mod == 3
				printf("Bsq%%Mod=\n");
				printv(2*(MOD.len+modn), Bsq);
				printf("res=\n");
				printv(2*(MOD.len+modn), res);
			#endif
		}
		#if verbose_exp_mod == 2
			printf("i=%d\nBsq%Mod=\n", i);
			printv(2*(MOD.len+modn), Bsq);
			printf("res=\n");
			printv(2*(MOD.len+modn), res);
		#endif
		
	}
	mod_fine(MOD.len+modn, res, MOD);

	memcpy(B, res, lenB*sizeof(res[0]));
	
	return 0;
}

uint32_t exp_mod_easy(const int lenB, uint32_t *B, const int lenE, const uint32_t *E, const int lenMOD, const uint32_t *MOD){
	
	uint32_t MOD_Data[2*lenMOD];
	ModKit kit = mod_prep(lenMOD, MOD, MOD_Data);
	return exp_mod(lenB, B, lenE, E, kit);
	
}

uint32_t mod_easy(const int lenA, uint32_t *A, const int lenMOD, const uint32_t *MOD){
	// B:= B % MOD, easy in the sense, that you don't have to setup the ModKit yourself.
	uint32_t MOD_Data[2*lenMOD];
	ModKit kit = mod_prep(lenMOD, MOD, MOD_Data);
	return mod(lenA, A, kit);
	
}

uint32_t hexify( char str_in[], uint32_t lenRes, uint32_t *Res){
	// res := atoi(str_in)

	
	#define verbose_hexify 0
	
	uint32_t l=strlen(str_in);
	

	uint32_t bl = 2;
	char str_temp[bl+1];
	str_temp[bl] = '\0';
	
	
	uint32_t base = 10; // The input string will be read as a base [base] number.
	uint32_t B[lenRes+1];//the current power of base
	uint32_t temp[lenRes+1];
	
	
	#if verbose_hexify
	
		B[0] = base*base*base*base;
		
		int c = 1;
		//Calculate log_2(base) (rounded up).
		while(B[0]>0){
			B[0] = B[0]>>1;
			c++;
		}
		//c /= 4;
		
		
		if( lenRes*8*sizeof(Res[0]) *4 < l*c){
			
			printf("In hexify, you are getting close to the max value for the result array.\n");
		}
	#endif
	
	
	memset(B, 0, sizeof(uint32_t)*(lenRes+1));
	memset(Res, 0, sizeof(uint32_t)*lenRes);
	memset(temp, 0, sizeof(uint32_t)*(lenRes+1));
	
	B[0] = base;
	temp[0] = -1;
	
	for( int i= 1; i<bl; i++){
		base *= B[0];
	
	}	
	B[0] = 1;
	
	for (int i=l-bl; i>=0; i -= bl){
		
		memcpy(str_temp, &str_in[i], bl*sizeof(char));

		temp[0] = atoi(str_temp);
		mult_array(1, temp, lenRes, B, lenRes+1, temp);
		
		mult_array(lenRes, B, 1, &base, lenRes+1, B);
		
		if(add_array(lenRes, Res, temp) || B[lenRes] != 0){
			return 1;
		}
		
	}
	
	if (l%bl!=0){
		
		memcpy(str_temp,str_in,(l%bl)*sizeof(char));
		str_temp[l%bl] = '\0';
		temp[0] = atoi(str_temp);
		mult_array(1, temp, lenRes, B, lenRes+1, temp);
		
		if(add_array(lenRes, Res, temp)){
			return 1;
		}
	
	}
	
	
	return 0;
	
}

#define verbose_new_int 0
uint32_t reduce(uint32_t lenI, uint32_t *I){
	for(int i = 0; i<lenI ; i++){
		for(int j=0; j< 8*sizeof(I[0]); j++){
			if((1<<j)&I[i] ){
				#if verbose_new_int
				printf("Reduce by=%x\n", (uint32_t) (i*sizeof(I[0])*8 + j));
				#endif
				right_shift_array(lenI, I, i, j);
				return j + lenI*sizeof(I[0])*8;
			}
		}
	}
	return ~0;
}

uint32_t new_int(const uint32_t lenI, uint32_t *I){
	
	#if verbose_new_int
	printf("new_int\nI=\n");
	printv(lenI, I);
	#endif
	uint32_t three = 3;
	uint32_t five = 5;
	uint32_t b[lenI+1];
	uint32_t Ilong[lenI+1];
	memset(b, 0, (lenI+1)*sizeof(b[0]));
	b[0] = 17;
	
	reduce(lenI, I);
	
	mult_array(lenI, I, 1, &five, lenI+1, Ilong);
	add_array(lenI+1, Ilong, b);
	reduce(lenI+1, Ilong);
	
	mult_array(lenI+1, Ilong, 1, &three, lenI+1, Ilong);
	add_array(lenI+1, Ilong, b);
	
	memcpy(I, Ilong, sizeof(I[0])*lenI);
	
	return Ilong[lenI];
	
}

uint32_t is_prime(uint32_t lenP, uint32_t *P){
	#define is_prime_verbose 0
	
	uint32_t P_Data[2*lenP];
	
	ModKit kit = mod_prep(lenP, P, P_Data);
	
	
	uint32_t q[kit.len];
	uint32_t fsig=~0;
	memset(q, 0, kit.len*sizeof(q[0]));
	
	if((P[0]&1)==0){
		#if is_prime_verbose
		printf("\nNot a prime[1]:\n");
		printv(kit.len, P);
		#endif
		return 0;
	}
	
	for(int i = 0; i<kit.len ; i++){
		for(int j=0; j< 8*sizeof(P[0]); j++){
			if((1<<j)&P[i] && (i != 0 || j!=0) ){
				fsig = 8*sizeof(P[0])*i + j;
				/*
				  q := (P-1)/2^(f_sig)
				  We don't have to explicitly subtract the 1, since it is deleted when shifting*/
				
				memcpy(q, P, kit.len*sizeof(P[0]));
				right_shift_array(kit.len, q, i, j);
				#if is_prime_verbose == 2
				printf("i=%d,\tj=%d\n", i,j);
				#endif
				i = kit.len;
				break;
			}
		}
	}
	#if is_prime_verbose== 2
	printf("q=\n");
	printv(kit.len, q);
	#endif
	
	uint32_t B[kit.len];
	uint32_t Bsq[kit.len*2];
	uint32_t maybe = 0;
	
	memset(B, 0, kit.len*sizeof(Bsq[0]));
	B[0] = 0x1601;
	for(int j=0; j<20;j++){
		// Guilty until proven Innocent
		maybe =0;
		#if is_prime_verbose == 2
		printf("B=\n");
		printv(kit.len, B);
		#endif
		
		mod(kit.len, B, kit);
		for(int j = 1; j< kit.len; j++){
			if(Bsq[j] == 0){
				maybe = 1;
				break;
			}
		}
		memcpy(Bsq, B, kit.len*sizeof(Bsq[0]));
		memset(&Bsq[kit.len], 0, kit.len*sizeof(Bsq[0]));
		
		// B^q (mod P), P prime => B^(q*2^i) = -1 (mod P) or B^q = 1 (mod P);
		exp_mod(kit.len, Bsq, kit.len, q, kit);
		
		memset(&Bsq[kit.len], 0, kit.len*sizeof(Bsq[0]));
		#if is_prime_verbose  == 2
		printf("B^q:\t");
		printv(kit.len, Bsq);
		#endif
		// Check if B = 1, or B = 0, if so skip to next B, because either B^q = 1 (mod P), or B|P, or B=0 i.e. B will not reveal, whether P is prime.
		if(Bsq[0] == 1||Bsq[0] == 0){
			maybe = 1;
			for(int j = 1; j< kit.len; j++){
				if(Bsq[j] != 0){
					maybe = 0;
					break;
				}
			}
		}
		
		if(maybe ==0){
			for(int i=0; i< fsig; i++){
				// Check if B^(q*2^i) = -1 (mod P) = P-1 (mod P)
				if(Bsq[0] == P[0]-1 && compare(kit.len-1, &P[1], &Bsq[1])==1){
					maybe = 1;
					break;
				}
				
				mult_array(kit.len, Bsq, kit.len, Bsq, 2*kit.len, Bsq);
				mod(2*kit.len, Bsq, kit);
				#if is_prime_verbose  == 2
				printf("B^(q*2^%d):\n", i+1);
				printv(kit.len, Bsq);
				#endif
			}
		}
		if(maybe==0){
			break;
		}
		new_int(kit.len, B);
	}
	
	#if is_prime_verbose
	if(maybe == 1){
		printf("\npotential prime:\n");
		printv(kit.len, P);
	}
	else{
		printf("\nNot a prime[2]:\n");
		printv(kit.len, P);
	}
	#endif
	return maybe;
}

#define ext_euclid_verbose 0

int euclid_step(int len, uint32_t *index, int *lenri,  uint32_t ri[4][len], uint32_t ai[2][len], uint32_t* d){
	for(int i=*lenri; i>0; i--){
		if(ri[(*index-1)%2][i-1] != 0){
			*lenri = i;
			break;
		}else if(i == 1 && ri[0] == 0){
			*lenri = 0;
		}
	}
	
	ModKit kit = mod_prep(*lenri, &ri[(*index)%2][0], &ri[2][0]);
	
	memset(d, 0, sizeof(d[0])*2*len);
	
	div_array(*lenri, &ri[(*index-1)%2][0], kit, *lenri, d);
	#if ext_euclid_verbose == 2
		uint32_t d_save[2*len];
		memcpy(d_save, d, sizeof(d[0])*2*len);
		printf("*lenri = %d\n", *lenri);
	#endif
	
	mult_array(len, d, len, &ai[(*index)%2][0], 2*len, d);
	add_array(len, &ai[(*index+1)%2][0], d);
	#if ext_euclid_verbose == 2
		printf("ri=\n");
		printv(len, &ri[(*index+1)%2][0]);
		printf("\nd=\n");
		printv(len, d_save);
		printf("ai\n");
		printv(len, &ai[(*index)%2][0]);
		printv(len, &ai[(*index+1)%2][0]);
	#endif
	for (int i=0; i<len; i++){
		if(ri[(*index-1)%2][i]!=0){
			#if ext_euclid_verbose ==2
			printf("gcd not found ri[%d]=%x\n", i, ri[(*index-1)%2][i]);
			#endif
			(*index) ++;
			return 0;
		}
	}
	#if ext_euclid_verbose==2
	printf("gcd found\n");
	#endif
	return 1;/*Only happens if ri == 0*/
}

uint32_t ext_euclid(const uint32_t len, const uint32_t * const A, const uint32_t * const B, uint32_t * const a, int lenGcd, uint32_t * const Gcd){
	// a := a, where A*a + B*b = gcd(A,B)
	// Gcd := gcd(A, B)
	
	#if ext_euclid_verbose
	printf("Euclidean algorithm\t");
	printf("\nA=\n");
	printv(len, A);
	printf("B=\n");
	printv(len, B);
	#endif
	uint32_t ri[4][len]; // 2 last dimensions for ModKit.
	uint32_t ai[2][len];
	uint32_t d[2*len];
	uint32_t index = 1;
	
	// ai[0] := 0
	// ai[1] := 1
	memset(ai, 0, 2*len*sizeof(ai[0][0]));
	ai[1][0] = 1;
	
	memset(d, 0, 2*len*sizeof(d[0]));
	
	memcpy(&ri[0][0], B, len*sizeof(A[0]));
	memcpy(&ri[1][0], A, len*sizeof(A[0]));
	
	index = compare(len, B, A);
	if(index==0){
		// B<A
		index = 2;
	}else if(index==2){
		// B>A
		index = 1;
	}else{
		printf("A = B");
		return ~0;
	}
	
	#if ext_euclid_verbose == 1
		printf("Start index =%d\n", index);
		uint32_t a_x_A[2*len];
		uint32_t a_x_A_mod[len];
		uint32_t B_Data[2*len];
		ModKit B_kit = mod_prep(len, B, B_Data);
	#endif
	
	
	#if ext_euclid_verbose ==2
	printf("Starting loop\n");
	#endif
	int lenri=len;
	uint32_t done = 0;
	while(done==0){
		#if ext_euclid_verbose ==2
		printf("\n\nindex=%d\n", index);
		#endif
		done = euclid_step(len, &index, &lenri, ri, ai, d);
		//mod(len, &ai[(index)%2][0], B_kit);
		
		#if ext_euclid_verbose >= 1
		
		mult_array(len, ai[(index)%2], len, A, 2*len, a_x_A);
		
		mod(2*len, a_x_A, B_kit);
		
		if(index%2 == 0){
			//a_x_A_mod := B - ai[(index)%2]*A
			memcpy(a_x_A_mod, B, sizeof(B[0])*len);
			sub_array(len, a_x_A_mod, a_x_A);
		}else{
			//a_x_A_mod := ai[(index)%2]*A
			memcpy(a_x_A_mod, a_x_A, sizeof(B[0])*len);
		}
		
		if(compare(len, a_x_A_mod, &ri[(index)%2][0] ) != 1){
			printf("\n\na*A != ri (mod B)\n\n");
			printf("\n(a*A) %% B=\n");
			printv(len, a_x_A_mod);
			return ~0;
		}
		#endif
		
	}
	
	if ( Gcd != NULL){
		if(lenGcd>=len){
			memcpy(Gcd, ri[(index)%2], sizeof(Gcd[0])*len);
		}else{
			memcpy(Gcd, ri[(index)%2], sizeof(Gcd[0])*lenGcd);
		}
	}
	
	if(index%2==0){
		#if ext_euclid_verbose == 1
			printf("a := B - a\n");
		#endif
		
		memcpy(a, B, len*sizeof(a[0]));
		sub_array(len, a, &ai[(index)%2][0]);
	}else{
		memcpy(a, &ai[(index)%2][0], sizeof(a[0])*len);
	}
	
	return lenri;
}

uint32_t invert_array(const uint32_t len, const uint32_t * const A, const uint32_t * const B, uint32_t * const a){
	// a:= A^-1 (mod B), in the sence that a*A = 1 (mod B) and 0<A<B
	uint32_t gcd;
	uint32_t lenri = ext_euclid(len, A, B, a, 1, &gcd);
	
	if(
		lenri == ~0
	){
		return 1;
	}
	
	if (lenri != 1 || gcd != 1){
		printf("Error: gcd!=1\n");
		return 1;
	}
	
	#if ext_euclid_verbose
		printf("gcd==1\n");
	#endif
	
	return 0;
}
