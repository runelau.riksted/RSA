# RSA implementation

An implementation of the RSA (Rivest–Shamir–Adleman) algorithm, see [Wikipedia]( https://en.wikipedia.org/wiki/RSA_(cryptosystem) ) for a theoretical discussion of the algorithm.

I implemented this algorithm to challenge myself and have fun. Thus I chose to implement everything from the ground up, most notably the necessary arithmetic functions for very large integers (>2048 bit).

This program fully implements the RSA algorithm including the key generation algorithm, and file encryption using a counter based mode of operation. However due to the nature of this project it obviously doesn't live up to good coding practices (don't write yourself what you can just import). It also doesn't implement the myriad of counter measures required to make RSA truly safe (Padding schemes, avoiding "bad" prime numbers, preventing timing attacks, extensive code reviews,...). Thus this disclaimer:

**Warning: This software should not be used for anything requiring actual security.**

\- But I assume you probably wouldn't rely on an encryption algorithm provided by a random internet stranger anyways ;-).

## Features:
- It supports almost arbitrary key sizes, default is 2048 bits. The number of bits just need to fit in an int, but keys above 8192 bits quickly become computationally intractable.
- It is fairly fast (for RSA). RSA isn't normally used for encrypting files, because it is much much slower than e.g. AES, but I can encrypt and decrypt at 45 KiB/s (2048 bit key). This is thanks to a lot of optimizing of my arithmetic library, and the implementation of the Chinese remainder theorem.
- It can encrypt and decrypt files using a counter based mode of operation, that is it does not use Electronic Code Book mode.
- It can generate RSA key pairs (from a given random seed), the private and public key can then be saved in separate files to be used later.
- (Almost) everything is "home made", only imports are <stdio.h>, <stdlib.h>, <string.h>, <stdint.h>, and <time.h> - some might consider this a bug rather than a feature...


# Requirements
You need a 64 bit operating system with a C compiler, or 64 bit Windows.

The software has been tested on Ubuntu 20/22 (GCC and Clang) and Windows 10 (see the included "crypt.exe" binary).

# How to setup and compile
Either make a Git clone of the repository or simply [download the project here](https://gitlab.gwdg.de/runelau.riksted/RSA/-/archive/master/RSA-master.zip) and unpack it.
On 64 bit Windows, you should be able to simply run the included executable "crypt.exe" in command prompt.

If you are not on Windows, but have a C compiler, then you can use the Makefile to compile the executable. With Make and GCC on path simply run:
```
make
```
This should create the "crypt" executable, if not you will need to edit the Makefile to use your compiler of choice first.

# Usage guide
All of the following commands are meant to be invoked in a terminal in the RSA directory, if you used "make" to compile the program you should replace "crypt.exe" with "./crypt" for all the following commands.

## Help
You can get usage help by invoking:
```
crypt.exe --help
```

## Generating keys
To generate a 2048 bit key use:
```
crypt.exe --keygen 0123456789 --bits 2048 --save key_file
```
Here with the example seed "0123456789" this number should ideally be much much longer and random. The private and public key will be saved to respectively "key_file.priv" and "key_file.pub".

## Encrypt file
To encrypt a file you need to generate or load a key (only the ".pub" file is necessary).
to load the public key in "key_file.pub" and encrypt "file.txt" invoke:
```
crypt.exe --load key_file --encrypt file.txt
```
the encrypted file will be stored as "file.txt.crypt". "file.txt" will not be altered.

## Decrypt file
To decrypt a file you need to load the private key associated with the public key used during the encryption step.
To load the private key in "key_file.priv" and decrypt "file.txt.crypt" invoke:
```
crypt.exe --load key_file --decrypt file.txt.crypt
```
The decrypted file will be stored as "file_out.txt". "file.txt.crypt" and "file.txt" will not be altered.

# Sending encrypted messages to internet strangers
As stated I do not recommend using this software for anything requiring actual security, but for illustrative purposes lets say Alice wants to send a file securely to Bob, how would she use this software to achieve that?

A short guide follows:

1. Both Alice and Bob download and [setup this software](#how-to-setup-and-compile). 
1. Bob then [generates a key pair](#generating-keys) and sends the public key file (ending in ".pub") to Alice.
1. Alice now loads Bob's public key, [encrypts her file](#encrypt-file) with it, and sends the resulting encrypted file (ending in .crypt) to Bob.
1. Bob loads his private key, and uses it to [decrypt Alice's file](#decrypt-file).
1. The unencrypted content of Alice's file can now be read from the produced file (ending in "_out"). 

The beauty of this scheme is, that no one other than Bob is able to read Alice's file - even if the attacker is able to read all the files transmitted between Alice and Bob. The reason is, that the Bob's public key only contains a recipe, that can be used to scramble messages, which is for all practical purposes not reversible - that is unless you know the mathematical secret in Bob's Private key (which he has not send to Alice), which allows him to unscramble Alice's encrypted file.

You can think of it as Bob buying a safe (the public key) along with it's key (the private key). Bob then sends the unlocked safe to Alice, while keeping the key himself. Alice then locks her message in Bob's safe and sends the locked safe back to Bob. When Bob receives the safe he can simply use his key to open the safe and get Alice's message.

This is an illustration of the beauty of Public key Cryptography, which we all use everyday, to create secure connections and verify the identity of websites and users alike on the internet.


# Feedback 
If you have any feedback or comment feel free to send it to me.
To be safe you should run:
```
crypt.exe --load author --encrypt Feedback.txt
```
On the file first and send me the encrypted version ;-).

# License and Liability
This program is distributed, because I think it is cool and not in the hope that it is useful. It is distributed WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

It should not be used as a substitute for a proper encryption algorithm. I cannot help you recover or restore any files encrypted, destroyed or corrupted by this software. I make no claim or guaranty as to the security of the encryption provided by this software.

You are free to use or share this software as you please.
